﻿using UnityEngine;
using sd = RotaryHeart.Lib.SerializableDictionary;
using System;
using BattleCity.Tanks;
using System.Collections.Generic;


namespace BattleCity
{
	[CreateAssetMenu]
	public sealed class GlobalAsset : ScriptableObject
	{
		#region Gun sprite/anim cho Tank
		[Serializable] public sealed class Direction_Sprite_Dict : sd.SerializableDictionaryBase<Direction, Sprite> { }
		[Serializable] public sealed class Color_Direction_Sprite_Dict : sd.SerializableDictionaryBase<Tank.Color, Direction_Sprite_Dict> { }
		public Color_Direction_Sprite_Dict tankGunSprites;

		[Serializable] public sealed class Direction_Anim_Dict : sd.SerializableDictionaryBase<Direction, RuntimeAnimatorController> { }
		[Serializable] public sealed class Color_Direction_Anim_Dict : sd.SerializableDictionaryBase<Tank.Color, Direction_Anim_Dict> { }
		public Color_Direction_Anim_Dict tankGunAnims;
		#endregion


		public TextAsset defaultCampaign;


		/// <summary>
		/// Bảng thông số chiến đấu của <see cref="Enemy"/>
		/// </summary>
		[Serializable]
		public struct EnemyStat
		{
			#region Thông số vật lý: tốc độ và độ trễ
			[Serializable] public sealed class Type_Float_Dict : sd.SerializableDictionaryBase<Enemy.Type, float> { }

			/// <summary>
			/// 0 &lt; value &lt;= 0.5f
			/// </summary>
			public Type_Float_Dict moveSpeed, bulletSpeed;

			/// <summary>
			/// 0 &lt; value
			/// </summary>
			public Type_Float_Dict delayShootSeconds;
			#endregion

			private static readonly Tank.Color[] ALL_COLORS = (Tank.Color[])Enum.GetValues(typeof(Tank.Color));

			#region Xác suất màu sắc (color)
			[Serializable] private sealed class Color_Vector2_Dict : sd.SerializableDictionaryBase<Tank.Color, Vector2> { }
			[Serializable] private sealed class Type_Color_Vector2_Dict : sd.SerializableDictionaryBase<Enemy.Type, Color_Vector2_Dict> { }

			/// <summary>
			/// Khoảng xác suất mỗi color = (x: min (inclusive), y:max (exclusive) )
			/// <para>Tất cả khoảng xác suất mỗi color liên tục nhau trong khoảng [0, 1F)</para>
			/// </summary>
			[SerializeField] private Type_Color_Vector2_Dict colorPossibilities;


			public Tank.Color GetEnemyColor(Enemy.Type type)
			{
				float value = UnityEngine.Random.Range(0f, 1f);
				foreach (var color_range in colorPossibilities[type])
					if (color_range.Value.x <= value && value < color_range.Value.y) return color_range.Key;
				throw new Exception();
			}
			#endregion

			#region Máu (colorHP)
			[Serializable] private sealed class Color_Byte_Dict : sd.SerializableDictionaryBase<Tank.Color, byte> { }
			[Serializable] private sealed class Type_Color_Byte_Dict : sd.SerializableDictionaryBase<Enemy.Type, Color_Byte_Dict> { }

			/// <summary>
			/// Máu của mỗi <see cref="color"/> khi: <c><see cref="color"/> != <see cref="Tank.Color.Red"/></c>
			/// </summary>
			[SerializeField] private Type_Color_Byte_Dict normalHP;


			public Dictionary<Tank.Color, int> GetEnemyColorHP(Tank.Color color, Enemy.Type type)
			{
				var result = new Dictionary<Tank.Color, int>();
				foreach (var c in ALL_COLORS) result[color] = 0;
				if (color != Tank.Color.Red)
				{
					foreach (var kvp in normalHP[type]) result[kvp.Key] = kvp.Value;
					return result;
				}

				switch (type)
				{
					case Enemy.Type.Small:
					case Enemy.Type.Fast: result[Tank.Color.White] = UnityEngine.Random.Range(0, 2); break;
					case Enemy.Type.Big:
						result[Tank.Color.Yellow] = UnityEngine.Random.Range(0, 2);
						goto case Enemy.Type.Fast;

					case Enemy.Type.Armored:
						result[Tank.Color.Green] = UnityEngine.Random.Range(0, 2);
						goto case Enemy.Type.Big;
				}
				return result;
			}
			#endregion
		}

		///<summary>
		/// Bảng thông số chiến đấu của <see cref="Enemy"/>
		/// </summary>
		[Tooltip("Bảng thông số chiến đấu của Enemy")]
		public EnemyStat enemyStat;
	}
}