﻿using UnityEngine;
using System;
using sd = RotaryHeart.Lib.SerializableDictionary;
using u = UnityEngine.InputSystem;


namespace BattleCity.Util
{
	[DisallowMultipleComponent]
	internal sealed class KeyBoard : GamepadComponent
	{
		#region Khởi tạo
		[Serializable] private struct Keymap { public u.Key L, R, U, D, A, B, X, Y, START, SELECT; }
		[Serializable] private sealed class Int_Keymap_Dict : sd.SerializableDictionaryBase<int, Keymap> { }
		[SerializeField] private Int_Keymap_Dict ALL_KEYMAPS;
		private Keymap keymap;


		private new void Awake()
		{
			base.Awake();
			keymap = ALL_KEYMAPS[gamepad.ID];
			keyboard = u.Keyboard.current;
		}
		#endregion


		#region Update gamepad
		private u.Keyboard keyboard;


		private void Update()
		{
			#region Xử lý kết nối / ngắt kết nối
			if (keyboard == null && (keyboard = u.Keyboard.current) == null)
			{
				if (currents[gamepad] == this)
				{
					currents[gamepad] = null;
					gamepad.__internal__Reset();
				}
				return;
			}

			if (!currents[gamepad]) currents[gamepad] = this;
			else if (currents[gamepad] != this) return;
			#endregion

			#region Dpad
			holdingDpads.Clear();
			CheckDpad(keymap.L, Direction.Left);
			CheckDpad(keymap.R, Direction.Right);
			CheckDpad(keymap.U, Direction.Up);
			CheckDpad(keymap.D, Direction.Down);
			if (holdingDpads.Count == 1) gamepad.__internal__UpdateDpad(Gamepad.ButtonState.HOLD, holdingDpads[0]);


			void CheckDpad(u.Key key, Direction direction)
			{
				var k = keyboard[key];
				if (k.wasPressedThisFrame) gamepad.__internal__UpdateDpad(Gamepad.ButtonState.PRESS, direction);
				else if (k.wasReleasedThisFrame) gamepad.__internal__UpdateDpad(Gamepad.ButtonState.RELEASE, direction);
				else if (k.isPressed) holdingDpads.Add(direction);
			}
			#endregion

			#region A, B, X, Y
			holdingABXY.Clear();
			CheckABXY(keymap.A, Gamepad.Button.A);
			CheckABXY(keymap.B, Gamepad.Button.B);
			CheckABXY(keymap.X, Gamepad.Button.X);
			CheckABXY(keymap.Y, Gamepad.Button.Y);
			if (holdingABXY.Count == 1) gamepad.__internal__UpdateABXY(Gamepad.ButtonState.HOLD, holdingABXY[0]);


			void CheckABXY(u.Key key, Gamepad.Button button)
			{
				var k = keyboard[key];
				if (k.wasPressedThisFrame) gamepad.__internal__UpdateABXY(Gamepad.ButtonState.PRESS, button);
				else if (k.wasReleasedThisFrame) gamepad.__internal__UpdateABXY(Gamepad.ButtonState.RELEASE, button);
				else if (k.isPressed) holdingABXY.Add(button);
			}
			#endregion

			if (keyboard[keymap.START].wasPressedThisFrame) gamepad.__internal__PressStart();
			if (keyboard[keymap.SELECT].wasPressedThisFrame) gamepad.__internal__PressSelect();
		}

		internal override void Vibrate(float speed, float delaySeconds) { }
		#endregion
	}
}