﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;
using sd = RotaryHeart.Lib.SerializableDictionary;
using BattleCity.Tanks;
using System.Collections.Generic;
using BattleCity.BattleTerrains;


namespace BattleCity.Util
{
	public sealed class InternalBattle : MonoBehaviour
	{
		[SerializeField] private Tilemap terrainMap, tankMap;
		[Serializable] private sealed class EnemyType_Byte_Dict : sd.SerializableDictionaryBase<Enemy.Type, byte> { }
		[SerializeField] private EnemyType_Byte_Dict enemyLifes;


		public Campaign.Battle ToBattle()
		{
			var battle = new Campaign.Battle();
			terrainMap.CompressBounds();
			var size = terrainMap.cellBounds.size;
			size.z = 0;
			var origin = terrainMap.origin;
			var cell = new Vector3Int();
			battle.map = new int[size.x][];
			int x, y;
			string NULL = ((int)BattleTerrain.Name.Null).ToString();

			tankMap.CompressBounds();
			tankMap.origin = terrainMap.origin;
			battle.playerSpawnIndexes = new Dictionary<Tank.Color, Vector3Int>();
			var enemySpawnList = new List<Vector3Int>();
			Vector3Int? eagleIndex = null;
			for (x = 0, cell.x = origin.x; x < size.x; ++x, ++cell.x)
			{
				battle.map[x] = new int[size.y];
				for (y = 0, cell.y = origin.y; y < size.y; ++y, ++cell.y)
				{
					var tile = terrainMap.GetTile(cell);
					int value = battle.map[x][y] = Convert.ToInt32(tile ? tile.name : NULL);
					if (value == (int)BattleTerrain.Name.Eagle)
					{
						if (eagleIndex != null) throw new Exception($"{name}: Chỉ được duy nhất 1 Eagle !");
						eagleIndex = new Vector3Int(x, y, 0);
					}

					#region Xuất playerSpawnIndexes và enemySpawnIndexes
					tile = tankMap.GetTile(cell);
					if (!tile) continue;
					switch (tile.name)
					{
						case "ENEMY": enemySpawnList.Add(new Vector3Int(x, y, 0).TerrainArray_To_TankArray()); break;
						case "YELLOW_PLAYER":
							if (battle.playerSpawnIndexes.ContainsKey(Tank.Color.Yellow)) throw new Exception($"{name}: Chỉ được duy nhất 1 yellowPlayer spawn index !");
							battle.playerSpawnIndexes[Tank.Color.Yellow] = new Vector3Int(x, y, 0).TerrainArray_To_TankArray();
							break;

						case "GREEN_PLAYER":
							if (battle.playerSpawnIndexes.ContainsKey(Tank.Color.Green)) throw new Exception($"{name}: Chỉ được duy nhất 1 greenPlayer spawn index !");
							battle.playerSpawnIndexes[Tank.Color.Green] = new Vector3Int(x, y, 0).TerrainArray_To_TankArray();
							break;
					}
					#endregion
				}
			}
			if (eagleIndex == null) throw new Exception($"{name}: Phải có 1 Eagle !");
			battle.enemySpawnIndexes = enemySpawnList.ToArray();


			if (name == "Battle 10 (Mappy)")
				;


			#region Kiểm tra hợp lệ playerSpawnIndexes và enemySpawnIndexes
			var list = new List<Vector3Int>() { eagleIndex.Value };
			foreach (Direction dir in Enum.GetValues(typeof(Direction)))
			{
				var p = (eagleIndex.Value + dir.ToUnitInt()).TerrainArray_To_TankArray();
				if (0 <= p.x && p.x < battle.map.Length && 0 <= p.y && p.y < battle.map[0].Length) list.Add(p);
			}

			foreach (var kvp in battle.playerSpawnIndexes)
				if (list.Contains(kvp.Value)) throw new Exception($"{name}: Player Spawn Index không được nằm trong < eagleIndex, eagleIndex + left/up/right/down > !");

			if (battle.enemySpawnIndexes.Length == 0) throw new Exception($"{name}: Phải có ít nhất 1 Enemy Spawn Index !");
			foreach (var i in battle.enemySpawnIndexes)
			{
				if (list.Contains(i)) throw new Exception($"{name}: Enemy Spawn Index không được nằm trong < eagleIndex, eagleIndex + left/up/right/down > !");
				foreach (var kvp in battle.playerSpawnIndexes)
					if (kvp.Value == i) throw new Exception($"{name}: Enemy Spawn Index không được trùng Player Spawn Index !");
			}
			#endregion


			battle.enemyLifes = new Dictionary<Enemy.Type, int>();
			foreach (var kvp in enemyLifes)
			{
				battle.enemyLifes[kvp.Key] = kvp.Value;
				battle.totalEnemy += kvp.Value;
			}
			return battle;
		}
	}
}