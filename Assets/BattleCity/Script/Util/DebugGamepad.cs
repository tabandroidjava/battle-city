﻿using UnityEngine;


namespace BattleCity.Util
{
	public class DebugGamepad : MonoBehaviour
	{
		protected void OnEnable()
		{
			Gamepad.instances[0].AddListener(listener0);
			Gamepad.instances[1].AddListener(listener1);
		}


		protected void OnDisable()
		{
			Gamepad.instances[0].RemoveListener(listener0);
			Gamepad.instances[1].RemoveListener(listener1);
		}


		private readonly Listener0 listener0 = new Listener0();
		private readonly Listener1 listener1 = new Listener1();

		private class Listener0 : IGamepadListener
		{
			public Gamepad.LocalState state { get; } = new Gamepad.LocalState();

			public void OnButtonA(Gamepad.ButtonState state)
			{
				print($"Gamepad 0: OnButtonA( {state} )");
			}


			public void OnButtonB(Gamepad.ButtonState state)
			{
				print($"Gamepad 0: OnButtonB( {state} )");
			}


			public void OnButtonSelect()
			{
				print("Gamepad 0: OnButtonSelect()");
			}


			public void OnButtonStart()
			{
				print("Gamepad 0: OnButtonStart()");
			}


			public void OnButtonX(Gamepad.ButtonState state)
			{
				print($"Gamepad 0: OnButtonX( {state} )");
			}


			public void OnButtonY(Gamepad.ButtonState state)
			{
				print($"Gamepad 0: OnButtonY( {state} )");
			}


			public void OnDpad(Direction direction, Gamepad.ButtonState state)
			{
				print($"Gamepad 0: OnDpad( {direction}, {state} )");
			}
		}

		private class Listener1 : IGamepadListener
		{
			public Gamepad.LocalState state { get; } = new Gamepad.LocalState();

			public void OnButtonA(Gamepad.ButtonState state)
			{
				print($"Gamepad 1: OnButtonA( {state} )");
			}


			public void OnButtonB(Gamepad.ButtonState state)
			{
				print($"Gamepad 1: OnButtonB( {state} )");
			}


			public void OnButtonSelect()
			{
				print("Gamepad 1: OnButtonSelect()");
			}


			public void OnButtonStart()
			{
				print("Gamepad 1: OnButtonStart()");
			}


			public void OnButtonX(Gamepad.ButtonState state)
			{
				print($"Gamepad 1: OnButtonX( {state} )");
			}


			public void OnButtonY(Gamepad.ButtonState state)
			{
				print($"Gamepad 1: OnButtonY( {state} )");
			}


			public void OnDpad(Direction direction, Gamepad.ButtonState state)
			{
				print($"Gamepad 1: OnDpad( {direction}, {state} )");
			}
		}
	}
}