﻿using UnityEngine;


namespace BattleCity.Util
{
	public sealed class InternalCampaign : MonoBehaviour
	{
		[SerializeField] private InternalBattle[] battles;


		public Campaign ToCampaign()
		{
			var campaign = new Campaign() { battles = new Campaign.Battle[battles.Length] };
			for (int i = 0; i < battles.Length; ++i)
				campaign.battles[i] = battles[i].ToBattle();
			return campaign;
		}
	}
}