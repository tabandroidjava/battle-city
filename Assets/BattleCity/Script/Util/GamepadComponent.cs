﻿using UnityEngine;
using System.Collections.Generic;
using UniRx.Async;
using System.Threading;


namespace BattleCity.Util
{
	/// <summary>
	/// Kéo vào <see cref="GameObject"/> chứa <see cref="Gamepad"/> và nhớ tắt đi (<see cref="GamepadComponent"/>.enable = <see langword="false"/>)
	/// </summary>
	[RequireComponent(typeof(Gamepad))]
	internal abstract class GamepadComponent : MonoBehaviour
	{
		protected Gamepad gamepad { get; private set; }
		protected readonly List<Direction> holdingDpads = new List<Direction>();
		protected readonly List<Gamepad.Button> holdingABXY = new List<Gamepad.Button>();


		protected void Awake()
		{
			currents[gamepad = GetComponent<Gamepad>()] = null;
		}


		internal static readonly Dictionary<Gamepad, GamepadComponent> currents = new Dictionary<Gamepad, GamepadComponent>();


		#region Vibrate()
		internal abstract void Vibrate(float speed, float delaySeconds);

		/// <summary>
		/// Field của <see cref="Vibrate"/>
		/// </summary>
		protected UniTask<bool> delayTask = UniTask.FromResult(false);

		/// <summary>
		/// Field của <see cref="Vibrate"/>
		/// </summary>
		protected float delay_StopTime;

		/// <summary>
		/// Nhớ gán <see cref="delayTask"/> = <see cref="Delay"/>
		/// <para>Return: <see langword="true"/> : Bị hủy.</para>
		/// </summary>
		protected async UniTask<bool> Delay(CancellationToken token = default)
		{
			using (var cts = CancellationTokenSource.CreateLinkedTokenSource(token, BattleField.Token))
			{
				return await UniTask.WaitWhile(() => Time.time < delay_StopTime, cancellationToken: cts.Token).SuppressCancellationThrow();
			}
		}
		#endregion
	}
}