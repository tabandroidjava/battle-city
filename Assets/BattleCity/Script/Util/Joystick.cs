﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading;
using u = UnityEngine.InputSystem;


namespace BattleCity.Util
{
	[DisallowMultipleComponent]
	internal sealed class Joystick : GamepadComponent
	{
		private new void Awake()
		{
			base.Awake();
			realDevices[this] = (null, null);
		}


		private void Update()
		{
			// Tạm thời chỉ hỗ trợ tay cầm chuẩn: Xbox, PS4

			CheckOfficialGamepad();
		}


		#region CheckOfficialGamepad()
		private static readonly Dictionary<Joystick, (u.Gamepad uGamepad, CancellationTokenSource cancelSource)> realDevices = new Dictionary<Joystick, (u.Gamepad uGamepad, CancellationTokenSource cancelSource)>();


		private void CheckOfficialGamepad()
		{
			var device = realDevices[this];
			if (device.uGamepad == null)
			{
				if (u.Gamepad.all.Count <= gamepad.ID) goto DEVICE_DISCONNECTED;
				for (int i = u.Gamepad.all.Count - 1; i >= 0; --i)
				{
					var g = u.Gamepad.all[i];
					foreach (var v in realDevices.Values) if (v.uGamepad == g) goto CONTINUE_LOOP_FIND_G;
					realDevices[this] = device = (g, new CancellationTokenSource());
					goto DEVICE_CONNECTED;
				CONTINUE_LOOP_FIND_G:;
				}

			DEVICE_DISCONNECTED:
				device.cancelSource?.Cancel();
				device.cancelSource?.Dispose();
				device.cancelSource = null;
				realDevices[this] = device;
				if (currents[gamepad] == this)
				{
					currents[gamepad] = null;
					gamepad.__internal__Reset();
				}
				return;
			}

		DEVICE_CONNECTED:
			if (!currents[gamepad]) currents[gamepad] = this;
			else if (currents[gamepad] != this)
			{
				currents[gamepad] = this;
				gamepad.__internal__Reset();
			}
			var uGamepad = device.uGamepad;

			#region Dpad and LeftStick
			holdingDpads.Clear();
			CheckDpad(uGamepad.dpad.left, uGamepad.leftStick.left, Direction.Left);
			CheckDpad(uGamepad.dpad.right, uGamepad.leftStick.right, Direction.Right);
			CheckDpad(uGamepad.dpad.up, uGamepad.leftStick.up, Direction.Up);
			CheckDpad(uGamepad.dpad.down, uGamepad.leftStick.down, Direction.Down);
			if (holdingDpads.Count == 1) gamepad.__internal__UpdateDpad(Gamepad.ButtonState.HOLD, holdingDpads[0]);


			void CheckDpad(u.Controls.ButtonControl dpadButton, u.Controls.ButtonControl stickButton, Direction direction)
			{
				if (gamepad.GetDpadState(direction) == Gamepad.ButtonState.RELEASE && (dpadButton.wasPressedThisFrame || stickButton.wasPressedThisFrame)) gamepad.__internal__UpdateDpad(Gamepad.ButtonState.PRESS, direction);
				else if ((dpadButton.wasReleasedThisFrame && !stickButton.isPressed) || (stickButton.wasReleasedThisFrame && !dpadButton.isPressed)) gamepad.__internal__UpdateDpad(Gamepad.ButtonState.RELEASE, direction);
				else if (dpadButton.isPressed || stickButton.isPressed) holdingDpads.Add(direction);
			}
			#endregion

			#region A, B, X, Y and RightStick
			holdingABXY.Clear();
			CheckABXY(uGamepad.aButton, uGamepad.rightStick.down, Gamepad.Button.A);
			CheckABXY(uGamepad.bButton, uGamepad.rightStick.right, Gamepad.Button.B);
			CheckABXY(uGamepad.xButton, uGamepad.rightStick.left, Gamepad.Button.X);
			CheckABXY(uGamepad.yButton, uGamepad.rightStick.up, Gamepad.Button.Y);
			if (holdingABXY.Count == 1) gamepad.__internal__UpdateABXY(Gamepad.ButtonState.HOLD, holdingABXY[0]);


			void CheckABXY(u.Controls.ButtonControl faceButton, u.Controls.ButtonControl stickButton, Gamepad.Button button)
			{
				if (gamepad.GetButtonState(button) == Gamepad.ButtonState.RELEASE && (faceButton.wasPressedThisFrame || stickButton.wasPressedThisFrame)) gamepad.__internal__UpdateABXY(Gamepad.ButtonState.PRESS, button);
				else if ((faceButton.wasReleasedThisFrame && !stickButton.isPressed) || (stickButton.wasReleasedThisFrame && !faceButton.isPressed)) gamepad.__internal__UpdateABXY(Gamepad.ButtonState.RELEASE, button);
				else if (faceButton.isPressed || stickButton.isPressed) holdingABXY.Add(button);
			}
			#endregion

			if (uGamepad.startButton.wasPressedThisFrame) gamepad.__internal__PressStart();
			if (uGamepad.selectButton.wasPressedThisFrame) gamepad.__internal__PressSelect();
		}
		#endregion


		internal override async void Vibrate(float speed, float delaySeconds)
		{
			var (uGamepad, cancelSource) = realDevices[this];
			if (uGamepad == null) return;
			uGamepad.SetMotorSpeeds(speed, speed);
			delay_StopTime = Time.time + delaySeconds;
			if (delayTask.IsCompleted)
			{
				var token = cancelSource.Token;
				await Delay(token);
				if (!token.IsCancellationRequested) uGamepad.SetMotorSpeeds(0, 0);
			}
		}
	}
}
