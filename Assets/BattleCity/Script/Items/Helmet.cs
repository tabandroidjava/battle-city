﻿using BattleCity.Tanks;
using UnityEngine;
using System.Threading;
using UniRx.Async;
using sd = RotaryHeart.Lib.SerializableDictionary;
using System;


namespace BattleCity.Items
{
	public sealed class Helmet : Item
	{
		[SerializeField] private float DELAY_SECONDS = 10;
		[Serializable] private sealed class TankColor_Anim_Dict : sd.SerializableDictionaryBase<Tank.Color, RuntimeAnimatorController> { }
		[SerializeField] private TankColor_Anim_Dict activeHelmets;


		public override async void OnCollision(Tank tank)
		{
			if (tank is Player)
			{
				#region Player lấy Helmet
				player = tank as Player;
				if (player.helmet)
				{
					player.helmet.delay_stopTime = Time.time + DELAY_SECONDS;
					Destroy(gameObject);
					return;
				}

				if (this == current) current = null;
				GetComponent<Animator>().runtimeAnimatorController = activeHelmets[player.color];
				transform.parent = player.transform;
				transform.localPosition = default;
				delay_stopTime = Time.time + DELAY_SECONDS;
				player.helmet = this;
				await WaitToDestroyHelmet();
				#endregion
			}
			else
			{
				// Enemy lấy Helmet
				Destroy(gameObject);
				Life.UpgradeEnemyColorAndHP();
			}
		}


		#region Async: Đợi và xóa helmet của Player
		private Player player;
		private CancellationTokenSource cancelSource;
		private float delay_stopTime;


		private void OnEnable()
		{
			cancelSource = new CancellationTokenSource();
		}


		private new void OnDisable()
		{
			base.OnDisable();
			cancelSource.Cancel();
			cancelSource.Dispose();
			if (player && this == player.helmet) player.helmet = null;
			Destroy(gameObject);
		}


		private async UniTask WaitToDestroyHelmet()
		{
			if (!await UniTask.WaitWhile(() => Time.time < delay_stopTime, PlayerLoopTiming.FixedUpdate, cancelSource.Token).SuppressCancellationThrow()) gameObject.SetActive(false);
		}
		#endregion
	}
}