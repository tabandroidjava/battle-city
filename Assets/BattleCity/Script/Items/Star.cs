﻿using BattleCity.Tanks;


namespace BattleCity.Items
{
	public sealed class Star : Item
	{
		public override void OnCollision(Tank tank)
		{
			Destroy(gameObject);
			if (tank is Player)
			{
				var player = tank as Player;
				++player.star;
				++player.fieryStar;
			}
			else foreach (var enemy in Enemy.enemies) if (enemy.weapon == Enemy.Weapon.Normal) enemy.weapon = Enemy.Weapon.Star;
		}
	}
}