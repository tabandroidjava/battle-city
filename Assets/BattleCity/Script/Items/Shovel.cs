﻿using BattleCity.Tanks;
using UnityEngine;
using BattleCity.BattleTerrains;
using System.Threading;
using UniRx.Async;
using System.Collections.Generic;
using System.Collections;


namespace BattleCity.Items
{
	public sealed class Shovel : Item
	{
		#region Cập nhật fenceIndexes (Tọa độ terrainIndex của hàng rào xung quanh Eagle)
		private static Fence fence;


		private void Awake()
		{
			if (fence?.eagleIndex != Eagle.Index) fence = new Fence(Eagle.Index);
		}
		#endregion


		[SerializeField] private float ENEMY_TASK_DELAY_SECONDS = 10, PLAYER_TASK_DELAY_SECONDS = 20;
		private static float delay_stopTime;
		private static CancellationTokenSource cancelSource;
		private static (UniTask task, bool playerPickShovel) taskInfo = (UniTask.CompletedTask, false);


		public override async void OnCollision(Tank tank)
		{
			Destroy(gameObject);
			if (tank is Player)
			{
				#region Player lấy Shovel
				delay_stopTime = Time.time + PLAYER_TASK_DELAY_SECONDS;

				if (!taskInfo.task.IsCompleted)
				{
					if (taskInfo.playerPickShovel)
						// Task của mình đang chạy
						return;

					// Task của đối thủ đang chạy
					cancelSource.Cancel();
					cancelSource.Dispose();
				}

				cancelSource = new CancellationTokenSource();
				await (taskInfo.task = UpdateFence(taskInfo.playerPickShovel = true));
				#endregion
			}
			else
			{
				#region Enemy lấy Shovel
				delay_stopTime = Time.time + ENEMY_TASK_DELAY_SECONDS;

				if (!taskInfo.task.IsCompleted)
				{
					if (!taskInfo.playerPickShovel)
						// Task của mình đang chạy
						return;

					// Task của đối thủ đang chạy
					cancelSource.Cancel();
					cancelSource.Dispose();
				}

				cancelSource = new CancellationTokenSource();
				await (taskInfo.task = UpdateFence(taskInfo.playerPickShovel = false));
				#endregion
			}


			async UniTask UpdateFence(bool playerPickShovel)
			{
				using (var cts = CancellationTokenSource.CreateLinkedTokenSource(cancelSource.Token, BattleField.Token))
				{
					var token = cts.Token;
					if (token.IsCancellationRequested) return;

					foreach (var p in fence)
						if (BattleTerrain.array[p.x][p.y]) Destroy(BattleTerrain.array[p.x][p.y].gameObject);

					if (playerPickShovel) fence.CreateSteel();

					if (await UniTask.WaitWhile(() => Time.time < delay_stopTime, PlayerLoopTiming.FixedUpdate, token).SuppressCancellationThrow()) return;

					if (playerPickShovel)
						foreach (var p in fence)
							if (BattleTerrain.array[p.x][p.y]) Destroy(BattleTerrain.array[p.x][p.y].gameObject);

					fence.CreateBrick();

					// Animation chớp gạch và delay (KHÔNG AWAIT)
					// task UpdateFence() bắt đầu chạy sẽ cancel task animation (nếu có)
				}
			}
		}



		private sealed class Fence : IEnumerable<Vector3Int>
		{
			public Vector3Int eagleIndex { get; private set; }

			private enum Direction
			{
				Left, LeftUp, Up, RightUp, Right, RightDown, Down, LeftDown
			}
			private static readonly IReadOnlyDictionary<Direction, Vector3Int> directions = new Dictionary<Direction, Vector3Int>()
			{
				[Direction.Left] = Vector3Int.left,
				[Direction.LeftUp] = Vector3Int.left + Vector3Int.up,
				[Direction.Up] = Vector3Int.up,
				[Direction.RightUp] = Vector3Int.right + Vector3Int.up,
				[Direction.Right] = Vector3Int.right,
				[Direction.RightDown] = Vector3Int.right + Vector3Int.down,
				[Direction.Down] = Vector3Int.down,
				[Direction.LeftDown] = Vector3Int.left + Vector3Int.down
			};
			private readonly Dictionary<Direction, Vector3Int> indexes = new Dictionary<Direction, Vector3Int>();



			public Fence(Vector3Int eagleIndex)
			{
				this.eagleIndex = eagleIndex;
				indexes.Clear();
				foreach (var kvp in directions)
				{
					var p = eagleIndex + kvp.Value;
					if (p.IsValidTerrainArray()) indexes[kvp.Key] = p;
				}
			}


			public void CreateBrick()
			{
				foreach (var kvp in indexes)
					switch (kvp.Key)
					{
						case Direction.Left: BattleTerrain.Create(BattleTerrain.Name.Brick_Right, kvp.Value); break;
						case Direction.LeftUp: BattleTerrain.Create(BattleTerrain.Name.Brick_Right_Down, kvp.Value); break;
						case Direction.Up: BattleTerrain.Create(BattleTerrain.Name.Brick_Down, kvp.Value); break;
						case Direction.RightUp: BattleTerrain.Create(BattleTerrain.Name.Brick_Left_Down, kvp.Value); break;
						case Direction.Right: BattleTerrain.Create(BattleTerrain.Name.Brick_Left, kvp.Value); break;
						case Direction.RightDown: BattleTerrain.Create(BattleTerrain.Name.Brick_Left_Up, kvp.Value); break;
						case Direction.Down: BattleTerrain.Create(BattleTerrain.Name.Brick_Up, kvp.Value); break;
						case Direction.LeftDown: BattleTerrain.Create(BattleTerrain.Name.Brick_Right_Up, kvp.Value); break;
					}
			}


			public void CreateSteel()
			{
				foreach (var kvp in indexes)
					switch (kvp.Key)
					{
						case Direction.Left: BattleTerrain.Create(BattleTerrain.Name.Steel_Right, kvp.Value); break;
						case Direction.LeftUp: BattleTerrain.Create(BattleTerrain.Name.Steel_Right_Down, kvp.Value); break;
						case Direction.Up: BattleTerrain.Create(BattleTerrain.Name.Steel_Down, kvp.Value); break;
						case Direction.RightUp: BattleTerrain.Create(BattleTerrain.Name.Steel_Left_Down, kvp.Value); break;
						case Direction.Right: BattleTerrain.Create(BattleTerrain.Name.Steel_Left, kvp.Value); break;
						case Direction.RightDown: BattleTerrain.Create(BattleTerrain.Name.Steel_Left_Up, kvp.Value); break;
						case Direction.Down: BattleTerrain.Create(BattleTerrain.Name.Steel_Up, kvp.Value); break;
						case Direction.LeftDown: BattleTerrain.Create(BattleTerrain.Name.Steel_Right_Up, kvp.Value); break;
					}
			}


			public IEnumerator<Vector3Int> GetEnumerator() => indexes.Values.GetEnumerator();

			IEnumerator IEnumerable.GetEnumerator() => indexes.Values.GetEnumerator();
		}
	}
}