﻿using UnityEngine;
using BattleCity.Tanks;
using System;
using System.Collections.Generic;
using BattleCity.BattleTerrains;


namespace BattleCity.Items
{
	[RequireComponent(typeof(SpriteRenderer), typeof(Animator))]
	[DisallowMultipleComponent]
	public abstract class Item : MonoBehaviour, ITankCollision
	{
		public const int BONUS_SCORE = 500;


		public enum Name : int
		{
			Grenade = 0,
			Gun = 1,
			Helmet = 2,
			Life = 3,
			Ship = 4,
			Shovel = 5,
			Star = 6,
			StopWatch = 7
		}


		public static Item current { get; protected set; }

		public IReadOnlyList<Vector3Int> tankCollisionIndexes { get; private set; }


		protected void OnDisable()
		{
			if (this == current) current = null;
		}


		#region Generate()
		/// <summary>
		/// Dùng cho thuật toán BFS
		/// </summary>
		private static bool[][] visited = Array.Empty<bool[]>();


		/// <summary>
		/// Thuật toán sinh <see cref="Item"/>, đưa vào môi trường và xử lý va chạm nếu có.
		/// </summary>
		public static void Generate()
		{
			Vector3Int index;

			#region Tìm index
			{
				var indexes = new List<Vector3Int>();

				#region BFS_AddToIndexes(): Thêm vị trí có thể sinh Item vào indexes
				#region Reset visited
				if (visited.Length != Tank.array.Length || visited[0].Length != Tank.array[0].Length)
				{
					visited = new bool[Tank.array.Length][];
					for (int x = visited.Length - 1, COLUMN = Tank.array[0].Length; x >= 0; --x) visited[x] = new bool[COLUMN];
				}
				else for (int x = visited.Length - 1; x >= 0; --x)
						for (int y = visited[0].Length - 1; y >= 0; --y) visited[x][y] = false;
				#endregion

				bool canDestroySteel, hasShip;

				// Danh sách điểm loại trừ (không thể sinh Item)
				var exclusives = new List<Vector3Int>();

				#region Khởi tạo exclusives
				foreach (var color_player in Player.players)
				{
					var player = color_player.Value;
					if (player) AddToExclusives(player.index);
					else AddToExclusives(BattleField.battle.playerSpawnIndexes[color_player.Key]);
				}
				if (Setting.current.enemy_Collise_item) foreach (var enemy in Enemy.enemies) AddToExclusives(enemy.index);


				void AddToExclusives(Vector3Int point)
				{
					if (!exclusives.Contains(point)) exclusives.Add(point);
					foreach (var d in Extensions.EIGHT_VECTORS)
					{
						var p = point + d;
						if (p.IsValidTankArray() && !exclusives.Contains(p)) exclusives.Add(p);
					}
				}
				#endregion

				var A = new List<Vector3Int>();
				var B = new List<Vector3Int>();
				var terrains = new List<BattleTerrain>();


				void BFS_AddToIndexes(Vector3Int _index)
				{
					A.Add(_index);
					bool isFirstLoop = true;
					do
					{
						foreach (var a in A)
							foreach (var d in Extensions.DIAGONAL_VECTORS)
							{
								var b = a + d;
								if (!b.IsValidTankArray() || visited[b.x][b.y]) continue;
								if (exclusives.Contains(b))
								{
									visited[b.x][b.y] = true;
									exclusives.Remove(b);
									if (!isFirstLoop) continue;
								}

								var _d = d.ToDirection();
								terrains.Clear();
								Tank.FindBlockable_BattleTerrain(a, _d, terrains);
								foreach (var terrain in terrains)
									if (terrain is Brick || (terrain is Steel && canDestroySteel)) continue;
									else if (terrain.IsBlockingTankMove(a, _d, hasShip)) goto CONTINUE_LOOP_DIRECTION;

								B.Add(b);
								if (!isFirstLoop)
								{
									visited[b.x][b.y] = true;
									indexes.Add(b);
								}

							CONTINUE_LOOP_DIRECTION:;
							}

						var tmp = B;
						B = A;
						A = tmp;
						B.Clear();
						isFirstLoop = false;
					} while (A.Count != 0);
				}
				#endregion

				#region Tìm và thêm vào indexes từ các vị trí Player tank/ Player Spawn
				foreach (var color_player in Player.players)
				{
					var player = color_player.Value;
					if (player)
					{
						canDestroySteel = player.star == 3;
						hasShip = player.ship;
						BFS_AddToIndexes(player.index);
					}
					else
					{
						canDestroySteel = false;
						hasShip = false;
						BFS_AddToIndexes(BattleField.battle.playerSpawnIndexes[color_player.Key]);
					}
				}
				#endregion

				index = (indexes.Count != 0) ? indexes[UnityEngine.Random.Range(0, indexes.Count)]
					: BattleField.battle.playerSpawnIndexes[UnityEngine.Random.Range(0, 2) == 0 ? Tank.Color.Yellow : Tank.Color.Green];
			}
			#endregion

			Item prefab = null;
			switch ((Name)UnityEngine.Random.Range(0, 8))
			{
				case Name.Grenade: prefab = Extensions.Load<Grenade>(); break;
				case Name.Gun: prefab = Extensions.Load<Gun>(); break;
				case Name.Helmet: prefab = Extensions.Load<Helmet>(); break;
				case Name.Life: prefab = Extensions.Load<Life>(); break;
				case Name.Ship: prefab = Extensions.Load<Ship>(); break;
				case Name.Shovel: prefab = Extensions.Load<Shovel>(); break;
				case Name.Star: prefab = Extensions.Load<Star>(); break;
				case Name.StopWatch: prefab = Extensions.Load<Stopwatch>(); break;
			}
			var item = Instantiate(prefab, index.TankArray_To_TankWorld(), Quaternion.identity);
			Effect.Play(Effect.Sound.Generate_Item);

			#region Khởi tạo Item và đưa vào môi trường
			if (current) Destroy(current.gameObject);
			current = item;
			item.transform.parent = BattleField.instance.itemAnchor;

			#region Khởi tạo tankCollisionIndexes
			var i = index - new Vector3Int(1, 1, 0);
			var list = new List<Vector3Int>();
			var dir = new Vector3Int();
			for (dir.x = 0; dir.x <= 2; ++dir.x)
				for (dir.y = 0; dir.y <= 2; ++dir.y)
				{
					var p = i + dir;
					if (p.IsValidTankArray()) list.Add(p);
				}
			item.tankCollisionIndexes = list;
			#endregion

			#region Xử lý va chạm Tank
			var tanks = new List<Tank>();
			foreach (var p in item.tankCollisionIndexes)
				foreach (var t in Tank.array[p.x][p.y])
					if ((Setting.current.enemy_Collise_item || !(t is Enemy)) && t.realMoveTask.IsCompleted) tanks.Add(t);
			if (tanks.Count != 0)
			{
				var tank = tanks[UnityEngine.Random.Range(0, tanks.Count)];
				item.OnCollision(tank);
				if (tank is Player) ScoreBoard.IncreaseScore(tank.color, BONUS_SCORE);
				Effect.Play(Effect.Sound.Tank_Collise_Item);
			}
			#endregion
			#endregion
		}
		#endregion


		public abstract void OnCollision(Tank tank);
	}
}