﻿/*
 * 
 * UniRX.Async.UniTask BUG: https://github.com/Cysharp/UniTask/issues/47
 * Fix tạm thời: tạo method / local function thay vì await UniTask.XXX(...)
 * 
 */


using BattleCity.Tanks;
using UnityEngine;
using UniRx.Async;
using System.Collections.Generic;
using BattleCity.AI;


namespace BattleCity.Items
{
	public sealed class Stopwatch : Item
	{
		#region PlayerTask fields
		private const float PLAYER_FREEZE_SECONDS = 10;
		private const int FLASH_DELAY_MILISECONDS = 300;
		private static float playerTask_stopTime;

		private static readonly Dictionary<Tank.Color, UniTask> playerTasks = new Dictionary<Tank.Color, UniTask>()
		{
			[Tank.Color.Yellow] = UniTask.CompletedTask,
			[Tank.Color.Green] = UniTask.CompletedTask
		};
		#endregion


		#region EnemyTask fields
		private const float ENEMY_FREEZE_SECONDS = 10;
		private static float enemyTask_stopTime;
		private static UniTask enemyTask = UniTask.CompletedTask;
		#endregion


		public override async void OnCollision(Tank tank)
		{
			Destroy(gameObject);
			if (tank is Enemy)
			{
				#region Đóng băng các Player hiện đang sống
				playerTask_stopTime = Time.time + PLAYER_FREEZE_SECONDS;
				for (int i = 0; i <= 1; ++i)
				{
					var color = (Tank.Color)i;
					if (Player.players[color] && playerTasks[color].IsCompleted) (playerTasks[color] = Freeze(Player.players[color])).CheckException();
				}


				async UniTask Freeze(Player player)
				{
					player.Freeze(true);
					var delay = UniTask.Delay(FLASH_DELAY_MILISECONDS, cancellationToken: player.Token).SuppressCancellationThrow();

					do
					{
						if (player.disableMovingTask.IsCompleted) player.spriteRenderer.enabled = false;
						if (await delay) return;
						if (player.disableMovingTask.IsCompleted) player.spriteRenderer.enabled = true;
						if (await delay) return;
					} while (Time.time < playerTask_stopTime);
					player.Freeze(false);
				}
				#endregion
			}
			else
			{
				#region Đóng băng các Enemy hiện tại và Enemy tương lai
				enemyTask_stopTime = Time.time + ENEMY_FREEZE_SECONDS;
				if (enemyTask.IsCompleted) await (enemyTask = Freeze());

				async UniTask Freeze()
				{
					EnemyAgent.isActiveAgent = false;
					await UniTask.WaitWhile(() => Time.time < enemyTask_stopTime, cancellationToken: BattleField.Token).SuppressCancellationThrow();
					EnemyAgent.isActiveAgent = true;
				}
				#endregion
			}
		}
	}
}