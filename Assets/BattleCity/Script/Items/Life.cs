﻿using BattleCity.Tanks;


namespace BattleCity.Items
{
	public sealed class Life : Item
	{
		public override void OnCollision(Tank tank)
		{
			Destroy(gameObject);
			if (tank is Player)
			{
				++Player.lifes[tank.color];
				Effect.Play(Effect.Sound.Life);
			}
			else UpgradeEnemyColorAndHP();
		}


		internal static void UpgradeEnemyColorAndHP()
		{
			foreach (var enemy in Enemy.enemies)
				++enemy.colorHP[enemy.color = enemy.color == Tank.Color.Red ? Tank.Color.Green : Tank.Color.Red];
		}
	}
}