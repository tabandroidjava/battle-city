﻿using BattleCity.Tanks;
using UnityEngine;
using UniRx.Async;
using System;
using System.Collections.Generic;
using sd = RotaryHeart.Lib.SerializableDictionary;


namespace BattleCity.Items
{
	public sealed class Ship : Item
	{
		[Serializable] private sealed class TankColor_Sprite_Dict : sd.SerializableDictionaryBase<Tank.Color, Sprite> { }
		[SerializeField] private TankColor_Sprite_Dict activeShips;
		[SerializeField] private SpriteRenderer spriteRenderer;


		public override async void OnCollision(Tank tank)
		{
			if (tank.ship)
			{
				if (tank is Player) Destroy(gameObject);
				else
				{
					var list = new List<Enemy>();
					foreach (var enemy in Enemy.enemies) if (!enemy.ship) list.Add(enemy);
					if (list.Count != 0) OnCollision(list[UnityEngine.Random.Range(0, list.Count)]);
					else Destroy(gameObject);
				}
				return;
			}

			current = null;
			SetActiveShip(tank.color);
			GetComponent<Animator>().enabled = false;
			transform.parent = tank.transform;
			transform.localPosition = default;
			tank.ship = this;

			if (tank is Enemy)
			{
				tank.transform.parent = null;
				DontDestroyOnLoad(tank);
			}

			// Không thể tắt ngay lập tức Animator !
			await UniTask.Yield();
			try { spriteRenderer.enabled = true; }
			catch (Exception) { }
		}


		public void SetActiveShip(Tank.Color color) => spriteRenderer.sprite = activeShips[color];
	}
}