﻿using BattleCity.Tanks;


namespace BattleCity.Items
{
	public sealed class Grenade : Item
	{
		public override void OnCollision(Tank tank)
		{
			Destroy(gameObject);
			if (tank is Player) while (Enemy.enemies.Count != 0) Enemy.enemies[0].Explode();
			else foreach (var color in Player.PLAYER_COLORS) if (Player.players[color]) Player.players[color].Explode();
			Effect.Play(Effect.Sound.Grenade);
		}
	}
}