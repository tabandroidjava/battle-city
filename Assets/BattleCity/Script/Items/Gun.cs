﻿using BattleCity.Tanks;


namespace BattleCity.Items
{
	public sealed class Gun : Item
	{
		public override void OnCollision(Tank tank)
		{
			Destroy(gameObject);
			if (tank is Player)
			{
				var player = tank as Player;
				player.star += 3;
				player.fieryStar += 3;
			}
			else
			{
				var enemy = tank as Enemy;
				if (enemy.weapon != Enemy.Weapon.Gun) enemy.weapon = Enemy.Weapon.Gun;
			}
		}
	}
}