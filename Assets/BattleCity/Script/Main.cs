﻿using UnityEngine;
using BattleCity.Tanks;
using sd = RotaryHeart.Lib.SerializableDictionary;
using System;
using UnityEngine.SceneManagement;
using BattleCity.AI;
using System.Collections.Generic;
using UnityEngine.UI;


namespace BattleCity
{
	public sealed class Main : MonoBehaviour
	{
		private void Awake()
		{
			if (Setting.current == null)
				if (PlayerPrefs.HasKey(Setting.KEY))
					Setting.current = Setting.Load(PlayerPrefs.GetString(Setting.KEY));
				else
				{
					PlayerPrefs.SetString(Setting.KEY, (Setting.current = Setting.DEFAULT).ToString());
					PlayerPrefs.Save();
				}
			highScoreText.text = ScoreBoard.highScore.ToString();
			menuIndex = @_menuIndex;
			Screen.orientation = ScreenOrientation.Landscape;
		}


		private void OnEnable()
		{
			var g = Gamepad.instances[0];
			g.onDpad += OnDpad;
			g.onButtonSelect += OnButtonSelect;
			g.onButtonStart += OnButtonStart;
			g.onButtonB += OnButtonB;
		}


		private void OnDisable()
		{
			var g = Gamepad.instances[0];
			g.onDpad -= OnDpad;
			g.onButtonSelect -= OnButtonSelect;
			g.onButtonStart -= OnButtonStart;
			g.onButtonB -= OnButtonB;
		}


		#region Điều khiển mũi tên
		[SerializeField] private Transform arrow;

		private enum Menu : int
		{
			OnePlayer = 0,
			TwoPlayer = 1,
			OnePlayer_OnePlayerAgent = 2,
			TwoPlayerAgent = 3,
			Setting = 4,
			Construction = 5
		}
		private static readonly int MENU_MAX_INDEX = Enum.GetValues(typeof(Menu)).Length - 1;
		[Serializable] private sealed class Menu_Vector3_Dict : sd.SerializableDictionaryBase<Menu, Vector3> { }
		[SerializeField] private Menu_Vector3_Dict menuPositions;

		private static int @_menuIndex;
		/// <summary>
		/// Di chuyển mũi tên lên: giảm value
		/// <para>Di chuyển mũi tên xuống: tăng value</para>
		/// <para>Nhớ khởi tạo <see cref="menuIndex"/> = <see cref="@_menuIndex"/></para>
		/// </summary>
		private int menuIndex
		{
			get => @_menuIndex;

			set
			{
				if (value < 0) value = MENU_MAX_INDEX;
				else if (value > MENU_MAX_INDEX) value = 0;
				@_menuIndex = value;
				arrow.position = menuPositions[(Menu)value];
			}
		}


		private void OnDpad(Direction direction, Gamepad.ButtonState state)
		{
			if (state == Gamepad.ButtonState.PRESS)
				if (direction == Direction.Up) --menuIndex;
				else if (direction == Direction.Down) ++menuIndex;
		}


		private void OnButtonSelect() => ++menuIndex;


		private void OnButtonStart()
		{
			var selected = (Menu)menuIndex;
			switch (selected)
			{
				case Menu.OnePlayer:
				case Menu.TwoPlayer:
				case Menu.OnePlayer_OnePlayerAgent:
				case Menu.TwoPlayerAgent:
					#region Chuẩn bị load Scene "BattleField"
					var pDict = new Dictionary<Tank.Color, Player>();

					#region Get player nếu không có, reset star & life theo Setting default, xóa PlayerAgent nếu có
					foreach (var color in Player.PLAYER_COLORS)
					{
						var p = pDict[color] = Player.players[color] ? Player.players[color] : Player.Get(color);
						p.star = Setting.current.defaultPlayerStar;
						Player.lifes[color] = Setting.current.defaultPlayerLife; ;
						if (PlayerAgent.agents[color])
						{
							Destroy(PlayerAgent.agents[color]);
							PlayerAgent.agents[color] = null;
						}
					}
					#endregion

					switch (selected)
					{
						case Menu.OnePlayer:
							Player.lifes[Tank.Color.Green] = 0;
							pDict[Tank.Color.Green].Recycle();
							break;

						case Menu.TwoPlayer:
							break;

						case Menu.OnePlayer_OnePlayerAgent:
							PlayerAgent.agents[Tank.Color.Green] = pDict[Tank.Color.Green].gameObject.AddComponent<PlayerAgent>();
							break;

						case Menu.TwoPlayerAgent:
							foreach (var p in pDict.Values) PlayerAgent.agents[p.color] = p.gameObject.AddComponent<PlayerAgent>();
							break;
					}
					SceneManager.LoadScene("BattleField");
					#endregion
					break;

				case Menu.Setting: SceneManager.LoadScene("Setting"); break;

				case Menu.Construction:
					throw new NotImplementedException();
			}
		}
		#endregion


		#region UI
		[SerializeField] private Text highScoreText;


		private void OnCloseButtonClick() => Application.Quit();

		private void OnButtonB(Gamepad.ButtonState state)
		{
			if (state == Gamepad.ButtonState.PRESS) Application.Quit();
		}
		#endregion
	}
}