﻿using UnityEngine;
using System;
using System.Collections.Generic;
using sd = RotaryHeart.Lib.SerializableDictionary;
using UniRx.Async;
using UnityEngine.SceneManagement;
using System.Threading;


namespace BattleCity
{
	/// <summary>
	/// Animation và Sound
	/// </summary>
	public sealed class Effect : MonoBehaviour
	{
		private static Effect instance;


		private void Awake()
		{
			if (instance) throw new Exception();
			DontDestroyOnLoad(instance = this);
			name = "Effect";
			animPool = new ObjectPool<Anim, Animator>(animPrefabs, animGlobalAnchor);
			soundPool = new ObjectPool<Sound, AudioSource>(soundPrefabs, soundGlobalAnchor);
		}


		private static CancellationTokenSource cancelSource = new CancellationTokenSource();

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void Init()
		{
			Sound[] keys = (Sound[])Enum.GetValues(typeof(Sound));
			foreach (var key in keys) loopedSounds[key] = false;

			SceneManager.activeSceneChanged += (Scene s1, Scene s2) =>
			{
				cancelSource.Cancel();
				cancelSource.Dispose();
				foreach (var key in keys)
					if (loopedSounds[key])
					{
						loopedSounds[key] = false;
						Recycle(key);
					}
				cancelSource = new CancellationTokenSource();

			};
			Instantiate(Extensions.Load<Effect>());
		}


		#region Animation Effect
		public enum Anim : int
		{
			Spawn,
			SmallExplosion,
			BigExplosion
		}

		[Serializable] private sealed class Anim_Animator_Dict : sd.SerializableDictionaryBase<Anim, Animator> { }
		[SerializeField] private Anim_Animator_Dict animPrefabs;
		[SerializeField] private Transform animGlobalAnchor;
		private static ObjectPool<Anim, Animator> animPool;


		public static async void Play(Anim key, Vector3 position)
		{
			var anim = animPool[key];
			anim.transform.position = position;
			await UniTask.Delay(Mathf.FloorToInt(anim.runtimeAnimatorController.animationClips[0].length * 1000), cancellationToken: cancelSource.Token).SuppressCancellationThrow();
			animPool[key] = anim;
		}
		#endregion


		#region Sound Effect
		public enum Sound : int
		{
			Bullet_Collise_Brick = 0,
			Bullet_Collise_Tank_But_Tank_Survive = 1,
			Bullet_Collise_Wall = 2,
			Button_Pause = 3,
			Eagle_Explosion = 4,
			Enemy_Exist = 5,
			Enemy_Explosion = 6,
			Game_Over = 7,
			Generate_Item = 8,
			Grenade = 9,
			High_Score = 10,
			Life = 11,
			Player_Explosion = 12,
			Player_Moving = 13,
			Player_Shoot = 14,
			Sand_Collise_Player = 15,
			Scoreboard_Bonus = 16,
			Scoreboard_Update_Score = 17,
			Start_BattleField = 18,
			Tank_Collise_Item = 19
		}

		[Serializable] private sealed class Sound_AudioSource_Dict : sd.SerializableDictionaryBase<Sound, AudioSource> { }
		[SerializeField] private Sound_AudioSource_Dict soundPrefabs;
		[SerializeField] private Transform soundGlobalAnchor;
		private static ObjectPool<Sound, AudioSource> soundPool;


		private static readonly Dictionary<Sound, bool> loopedSounds = new Dictionary<Sound, bool>();

		/// <param name="loop">if <see langword="true"/> : Chỉ được recycle khi gọi <see cref="RecycleSound"/> hoặc khi <see cref="Scene"/> thay đổi.</param>
		public static AudioSource Play(Sound key, bool loop = false)
		{
			var source = soundPool[key];
			source.loop = loop;
			if (!loop) WaitToRecycle();
			else loopedSounds[key] = true;
			return source;

			async void WaitToRecycle()
			{
				await UniTask.Delay(Mathf.FloorToInt(source.clip.length * 1000), cancellationToken: cancelSource.Token).SuppressCancellationThrow();
				soundPool[key] = source;
			}
		}


		/// <param name="source">Nếu <see langword="null"/> : recycle tất cả obj nếu có tương ứng <paramref name="key"/></param>
		public static void Recycle(Sound key, AudioSource source = null)
		{
			soundPool[key] = source;
		}
		#endregion


		private sealed class ObjectPool<TKey, TValue> where TKey : Enum where TValue : Component
		{
			private readonly IReadOnlyDictionary<TKey, TValue> prefabs;
			private readonly IReadOnlyDictionary<TKey, Transform> anchors;
			private readonly IReadOnlyDictionary<TKey, List<TValue>> used, free;


			public ObjectPool(IDictionary<TKey, TValue> prefabs, Transform globalAnchor)
			{
				var p = new Dictionary<TKey, TValue>();
				foreach (var kvp in prefabs) p[kvp.Key] = kvp.Value;
				this.prefabs = p;
				var anchors = new Dictionary<TKey, Transform>();
				var used = new Dictionary<TKey, List<TValue>>();
				var free = new Dictionary<TKey, List<TValue>>();
				foreach (TKey key in Enum.GetValues(typeof(TKey)))
				{
					var a = anchors[key] = new GameObject().transform;
					a.parent = globalAnchor;
					a.name = key.ToString();
					used[key] = new List<TValue>();
					free[key] = new List<TValue>();
				}
				this.anchors = anchors;
				this.used = used;
				this.free = free;
			}


			/// <summary>
			/// Get: Lấy <typeparamref name="TValue"/> từ Pool và enable gameObject.
			/// <para>Set: thu hồi <typeparamref name="TValue"/> về Pool và disable gameObject.</para>
			/// <para>Nếu set value == <see langword="null"/> : Thu hồi tất cả <typeparamref name="TValue"/>.</para>
			/// </summary>
			public TValue this[TKey key]
			{
				get
				{
					TValue obj;
					var free = this.free[key];
					if (free.Count != 0)
					{
						obj = free[0];
						free.RemoveAt(0);
					}
					else obj = Instantiate(prefabs[key], anchors[key]);

					used[key].Add(obj);
					obj.gameObject.SetActive(true);
					return obj;
				}

				set
				{
					if (value)
					{
						value.gameObject.SetActive(false);
						used[key].Remove(value);
						free[key].Add(value);
					}
					else
					{
						var used = this.used[key];
						var free = this.free[key];
						foreach (var obj in used)
						{
							obj.gameObject.SetActive(false);
							free.Add(obj);
						}
						used.Clear();
					}
				}
			}
		}
	}
}