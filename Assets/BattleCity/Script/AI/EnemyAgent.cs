﻿using UnityEngine;
using BattleCity.Tanks;
using UniRx.Async;
using System.Collections.Generic;
using System;


namespace BattleCity.AI
{
	[RequireComponent(typeof(Enemy))]
	public sealed class EnemyAgent : TankAgent<Enemy>
	{
		#region isActiveAgent
		private static readonly List<EnemyAgent> disabledAgents = new List<EnemyAgent>();
		private static bool _isActiveAgent = true;
		/// <summary>
		/// Kích hoạt <see cref="EnemyAgent"/> đồng thời kích hoạt <see cref="Enemy"/> tương ứng ?
		/// <para>value == <see langword="true"/> : Có thể Move và Shoot. Enable các <see cref="EnemyAgent"/> hiện tại và tương lai.</para>
		/// <para>value == <see langword="false"/> : Không thể Move và Shoot. Disable các <see cref="EnemyAgent"/> hiện tại và tương lai.</para>
		/// </summary>
		public static bool isActiveAgent
		{
			get => _isActiveAgent;
			set
			{
				if (_isActiveAgent = value)
				{
					foreach (var a in disabledAgents) if (a) a.enabled = value;
					disabledAgents.Clear();
				}
				else if (disabledAgents.Count == 0) foreach (var enemy in Enemy.enemies)
					{
						enemy.agent.enabled = value;
						disabledAgents.Add(enemy.agent);
					}
			}
		}


		private void OnEnable()
		{
			if (!isActiveAgent)
			{
				enabled = false;
				disabledAgents.Add(this);
			}
		}
		#endregion


		/// <summary>
		/// Khởi tạo <see cref="Enemy.lifes"/> và spawn số lượng <see cref="Enemy"/> ban đầu.
		/// </summary>
		public static void Init()
		{
			bool hasTwoPlayer = (Player.players[Tank.Color.Yellow] || Player.lifes[Tank.Color.Yellow] != 0)
					&& (Player.players[Tank.Color.Green] || Player.lifes[Tank.Color.Green] != 0);

			int count = 0;
			switch (Setting.current.AI.difficulty)
			{
				case AISetting.Difficulty.Easy:
					count = hasTwoPlayer ? 6 : 3;
					break;

				case AISetting.Difficulty.Medium:
					count = hasTwoPlayer ? 10 : 5;
					break;

				case AISetting.Difficulty.Hard:
					count = hasTwoPlayer ? 14 : 7;
					break;
			}

			foreach (Enemy.Type type in Enum.GetValues(typeof(Enemy.Type)))
				Enemy.lifes[type] = BattleField.battle.enemyLifes?.ContainsKey(type) == true ? BattleField.battle.enemyLifes[type] : 0;

			for (int i = Mathf.Min(count, Enemy.lifes.totalLife); i > 0; --i) SpawnEnemy().CheckException();
			if (Enemy.lifes.totalLife != 0) Effect.Play(Effect.Sound.Enemy_Exist, loop: true);
		}


		public static async UniTask<Enemy> SpawnEnemy()
		{
			//debug
			var types = new List<Enemy.Type>();
			foreach (var kvp in Enemy.lifes)
				if (kvp.Value != 0) types.Add(kvp.Key);

			if (types.Count == 0) return null;
			var data = new Enemy.InitData()
			{
				index = BattleField.battle.enemySpawnIndexes[UnityEngine.Random.Range(0, BattleField.battle.enemySpawnIndexes.Length)],
				type = types[UnityEngine.Random.Range(0, types.Count)]
			};
			var stat = Extensions.Load<GlobalAsset>().enemyStat;
			data.color = stat.GetEnemyColor(data.type);
			data.colorHP = stat.GetEnemyColorHP(data.color, data.type);
			return await Enemy.Spawn(data, 1000);
		}


		// debug
		private void Update()
		{
			RandomMove();
			RandomShoot();
		}
	}
}