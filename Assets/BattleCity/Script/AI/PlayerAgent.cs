﻿using UnityEngine;
using BattleCity.Tanks;
using System.Collections.Generic;


namespace BattleCity.AI
{
	[RequireComponent(typeof(Player))]
	public sealed class PlayerAgent : TankAgent<Player>
	{
		public static readonly Dictionary<Tank.Color, PlayerAgent> agents = new Dictionary<Tank.Color, PlayerAgent>()
		{
			[Tank.Color.Yellow] = null,
			[Tank.Color.Green] = null
		};


		public static async void DecideBorrowingLife(Tank.Color color)
		{
			// Quyết định xem có mượn mạng đồng đội ?

			await Player.Spawn(color, 1000);
		}


		// debug
		private void Update()
		{
			RandomShoot();
			if (tank.disableMovingTask.IsCompleted) RandomMove();
		}
	}
}