﻿using UnityEngine;
using System;
using Newtonsoft.Json;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


namespace BattleCity.AI
{
	[Serializable]
	public sealed class AISetting
	{
		public enum Difficulty : int
		{
			Easy = 0, Medium = 1, Hard = 2
		}
		public Difficulty difficulty;


		public static AISetting DEFAULT => new AISetting()
		{
			difficulty = Difficulty.Easy
		};

		/// <summary>
		/// Serialize this to json
		/// </summary>
		public override string ToString() => JsonConvert.SerializeObject(this);

		public static AISetting Load(string json) => JsonConvert.DeserializeObject<AISetting>(json);
	}



	public sealed class AISettingUI : MonoBehaviour
	{
		[SerializeField] private Slider difficulty;
		[SerializeField] private Text difficulty_Label;
		[SerializeField] private Button closeButton;


		private void Awake()
		{
			var ai = Setting.current.AI;
			difficulty.value = (float)ai.difficulty;
			difficulty_Label.text = ai.difficulty.ToString();

			difficulty.onValueChanged.AddListener((float value) => difficulty_Label.text = (ai.difficulty = (AISetting.Difficulty)value).ToString());
			closeButton.onClick.AddListener(() => SceneManager.LoadScene("Setting"));
		}


		private void OnEnable()
		{
			var g = Gamepad.instances[0];
			g.onButtonB += OnButtonB;
		}


		private void OnDisable()
		{
			var g = Gamepad.instances[0];
			g.onButtonB -= OnButtonB;
		}


		private void OnButtonB(Gamepad.ButtonState state)
		{
			if (state == Gamepad.ButtonState.PRESS) SceneManager.LoadScene("Setting");
		}
	}
}
