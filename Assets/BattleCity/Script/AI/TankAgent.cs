﻿using UnityEngine;
using BattleCity.Tanks;


namespace BattleCity.AI
{
	/// <summary>
	/// Chỉ điều khiển <see cref="Tank"/>, thay đổi môi trường khi ở Update() hoặc FixedUpdate() hoặc khi môi trường gây ra sync-event !
	/// <para>Không được enable/disable gameObject !</para>
	/// </summary>
	[RequireComponent(typeof(Tank))]
	public abstract class TankAgent<T> : MonoBehaviour where T : Tank
	{
		protected T tank;


		protected void Awake()
		{
			tank = GetComponent<T>();
		}


		protected void RandomMove()
		{
			if (tank.moveTask.IsCompleted && UnityEngine.Random.Range(0, 2) == 0)
			{
				if (UnityEngine.Random.Range(0, 2) == 0)
					switch (UnityEngine.Random.Range(0, 4))
					{
						case 0: tank.direction = Direction.Left; break;
						case 1: tank.direction = Direction.Right; break;
						case 2: tank.direction = Direction.Up; break;
						case 3: tank.direction = Direction.Down; break;
					}

				(tank.moveTask = tank.Move()).CheckException();
			}
		}


		protected void RandomShoot()
		{
			if (tank.canShootBullets > 0 && UnityEngine.Random.Range(0, 2) == 0) tank.Shoot().CheckException();
		}
	}
}