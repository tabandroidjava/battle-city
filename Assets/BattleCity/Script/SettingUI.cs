﻿using UnityEngine;
using BattleCity.AI;
using System;
using System.Collections.Generic;
using BattleCity.Tanks;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace BattleCity
{
	/// <summary>
	/// Load ở scene "Main" và chỉnh sửa ở scene "Setting"
	/// <para>Nhớ gán <see cref="current"/></para>
	/// </summary>
	[Serializable]
	public sealed class Setting
	{
		public static readonly string KEY = typeof(Setting).ToString();

		/// <summary>
		/// 0 &lt;= value &lt;= 3
		/// </summary>
		public byte defaultPlayerStar;
		public bool playerBullet_Collise_player;
		public bool enemy_Collise_item;
		public int defaultPlayerLife;
		public Campaign campaign;
		public AISetting AI;


		public static Setting current;

		public static Setting DEFAULT => new Setting()
		{
			playerBullet_Collise_player = false,
			enemy_Collise_item = true,
			defaultPlayerLife = 2, // debug
			campaign = Campaign.Load(Extensions.Load<GlobalAsset>().defaultCampaign.text),
			AI = AISetting.DEFAULT
		};

		/// <summary>
		/// Serialize this to json
		/// </summary>
		public override string ToString() => JsonConvert.SerializeObject(this);

		public static Setting Load(string json) => JsonConvert.DeserializeObject<Setting>(json);
	}



	// Sẽ có bug nếu implement IEnumerable<T>
	[Serializable]
	public sealed class Campaign
	{
		[Serializable]
		public sealed class Battle
		{
			/// <summary>
			/// Phải có và có duy nhất 1 Eagle !
			/// <para>Size= (20, 14) hiển thị tối ưu nhất cho màn hình PC 1920x1080</para>
			/// </summary>
			public int[][] map;

			/// <summary>
			/// <para>Phải có đủ 2 spawn index</para>
			///  Không nằm trong { eagleIndex, eagleIndex + left/up/right/down }
			/// </summary>
			public Dictionary<Tank.Color, Vector3Int> playerSpawnIndexes;

			/// <summary>
			/// <para>Phải có ít nhất 1 spawn index</para>
			/// Không nằm trong { eagleIndex,
			/// eagleIndex + left/up/right/down } và playerSpawnIndexes
			/// </summary>
			public Vector3Int[] enemySpawnIndexes;
			public Dictionary<Enemy.Type, int> enemyLifes;
			public int totalEnemy;
		}
		public Battle[] battles;


		public IEnumerator<Battle> GetEnumerator()
		{
			while (true) foreach (var battle in battles) yield return battle;
		}


		/// <summary>
		/// Serialize this to json
		/// </summary>
		public override string ToString() => JsonConvert.SerializeObject(this);

		public static Campaign Load(string json) => JsonConvert.DeserializeObject<Campaign>(json);
	}



	public sealed class SettingUI : MonoBehaviour
	{
		#region Khởi tạo
		[SerializeField] private Slider defaultPlayerStar, defaultPlayerLife;
		[SerializeField] private Text defaultPlayerStar_Label, defaultPlayerLife_Label;
		[SerializeField] private Toggle playerBulletCollisePlayer, enemyColliseItem;
		[SerializeField] private Dropdown campaign;
		[SerializeField] private Button aiSettingButton, closeButton, resetDataButton;


		private void Awake()
		{
			var setting = Setting.current;
			defaultPlayerStar.value = setting.defaultPlayerStar;
			defaultPlayerStar_Label.text = $"Default Player Star: {setting.defaultPlayerStar}";
			defaultPlayerLife.value = setting.defaultPlayerLife;
			defaultPlayerLife_Label.text = $"Default Player Life: {setting.defaultPlayerLife}";
			playerBulletCollisePlayer.isOn = setting.playerBullet_Collise_player;
			enemyColliseItem.isOn = setting.enemy_Collise_item;
			//campaign.value=

			defaultPlayerStar.onValueChanged.AddListener((float value) => defaultPlayerStar_Label.text = $"Default Player Star: {setting.defaultPlayerStar = (byte)value}");
			defaultPlayerLife.onValueChanged.AddListener((float value) => defaultPlayerLife_Label.text = $"Default Player Life: {setting.defaultPlayerLife = (byte)value}");
			playerBulletCollisePlayer.onValueChanged.AddListener((bool value) => setting.playerBullet_Collise_player = value);
			enemyColliseItem.onValueChanged.AddListener((bool value) => setting.enemy_Collise_item = value);
			campaign.onValueChanged.AddListener((int value) => throw new NotImplementedException());
			aiSettingButton.onClick.AddListener(() => SceneManager.LoadScene("AISetting"));
			closeButton.onClick.AddListener(() => SceneManager.LoadScene("Main"));
			resetDataButton.onClick.AddListener(() =>
			{
				PlayerPrefs.DeleteAll();
				Setting.current = Setting.DEFAULT;
				SceneManager.LoadScene("Setting");
			});
		}


		private void OnEnable()
		{
			var g = Gamepad.instances[0];
			g.onButtonB += OnButtonB;
		}


		private void OnDisable()
		{
			var g = Gamepad.instances[0];
			g.onButtonB -= OnButtonB;
			PlayerPrefs.SetString(Setting.KEY, Setting.current.ToString());
			PlayerPrefs.Save();
		}
		#endregion


		private void OnButtonB(Gamepad.ButtonState state)
		{
			if (state == Gamepad.ButtonState.PRESS) SceneManager.LoadScene("Main");
		}
	}
}