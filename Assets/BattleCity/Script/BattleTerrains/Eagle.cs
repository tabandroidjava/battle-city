﻿using BattleCity.Tanks;
using UnityEngine;
using System;
using BattleCity.AI;


namespace BattleCity.BattleTerrains
{
	[RequireComponent(typeof(SpriteRenderer))]
	public sealed class Eagle : BattleTerrain
	{
		private static Eagle instance;

		[SerializeField] private Sprite dead;
		[SerializeField] private SpriteRenderer spriteRenderer;
		public static Vector3Int Index => instance.index;



		private void Awake()
		{
			if (instance) throw new Exception("Chỉ được tạo 1 Eagle !");
			instance = this;
		}


		public override bool IsBlockingTankMove(Vector3Int tankIndex, Direction tankDirecton, bool tankHasShip) => true;


		public override Bullet.CollisionResult OnCollision(Bullet bullet)
		{
			if (!BattleField.isGameOver) BattleField.Finish(isGameOver: true).CheckException();
			if (spriteRenderer.sprite != dead)
			{
				spriteRenderer.sprite = dead;
				Effect.Play(Effect.Anim.BigExplosion, transform.position);
				Effect.Play(Effect.Sound.Eagle_Explosion);
				foreach (var color in Player.PLAYER_COLORS) if (!PlayerAgent.agents[color]) Gamepad.instances[(int)color].Vibrate(1, 3);
				return Bullet.CollisionResult.RemoveBullet;
			}
			return Bullet.CollisionResult.KeepBullet;
		}
	}
}