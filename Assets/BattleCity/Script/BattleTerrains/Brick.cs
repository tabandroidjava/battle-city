﻿using BattleCity.Tanks;
using UnityEngine;


namespace BattleCity.BattleTerrains
{
	public sealed class Brick : BattleTerrain
	{
		[SerializeField] private SerializedArray2D particles;


		private void Awake()
		{
			switch (name)
			{
				case Name.Brick_Full: break;

				case Name.Brick_Left: particles.DeleteItems(2, 3, 0, 3); break;
				case Name.Brick_Right: particles.DeleteItems(0, 1, 0, 3); break;
				case Name.Brick_Up: particles.DeleteItems(0, 3, 0, 1); break;
				case Name.Brick_Down: particles.DeleteItems(0, 3, 2, 3); break;

				case Name.Brick_Left_Up:
					particles.DeleteItems(0, 3, 0, 1);
					particles.DeleteItems(2, 3, 2, 3);
					break;

				case Name.Brick_Right_Up:
					particles.DeleteItems(0, 3, 0, 1);
					particles.DeleteItems(0, 1, 2, 3);
					break;

				case Name.Brick_Right_Down:
					particles.DeleteItems(0, 3, 2, 3);
					particles.DeleteItems(0, 1, 0, 1);
					break;

				case Name.Brick_Left_Down:
					particles.DeleteItems(0, 3, 2, 3);
					particles.DeleteItems(2, 3, 0, 1);
					break;
			}
		}


		public override bool IsBlockingTankMove(Vector3Int tankIndex, Direction tankDirecton, bool tankHasShip)
		{
			var p = tankIndex - index.TerrainArray_To_TankArray();
			switch (tankDirecton)
			{
				case Direction.Left:
					//A
					if (p.x == 2 && p.y == 1) return ExistPiece(2, 3, 2, 3);
					//B
					if (p.x == 2 && p.y == 0) return ExistPiece(2, 3, 0, 1) || ExistPiece(2, 3, 2, 3);
					//C
					if (p.x == 2 && p.y == -1) return ExistPiece(2, 3, 0, 1);
					//D
					if (p.x == 1 && p.y == 1) return ExistPiece(0, 1, 2, 3);
					//E
					if (p.x == 1 && p.y == 0) return ExistPiece(0, 1, 0, 1) || ExistPiece(0, 1, 2, 3);
					//F
					if (p.x == 1 && p.y == -1) return ExistPiece(0, 1, 0, 1);
					break;

				case Direction.Right:
					//A
					if (p.x == -2 && p.y == 1) return ExistPiece(0, 1, 2, 3);
					//B
					if (p.x == -2 && p.y == 0) return ExistPiece(0, 1, 0, 1) || ExistPiece(0, 1, 2, 3);
					//C
					if (p.x == -2 && p.y == -1) return ExistPiece(0, 1, 0, 1);
					//D
					if (p.x == -1 && p.y == 1) return ExistPiece(2, 3, 2, 3);
					//E
					if (p.x == -1 && p.y == 0) return ExistPiece(2, 3, 0, 1) || ExistPiece(2, 3, 2, 3);
					//F
					if (p.x == -1 && p.y == -1) return ExistPiece(2, 3, 0, 1);
					break;

				case Direction.Up:
					//A
					if (p.x == -1 && p.y == -2) return ExistPiece(0, 1, 0, 1);
					//B
					if (p.x == 0 && p.y == -2) return ExistPiece(0, 1, 0, 1) || ExistPiece(2, 3, 0, 1);
					//C
					if (p.x == 1 && p.y == -2) return ExistPiece(2, 3, 0, 1);
					//D
					if (p.x == -1 && p.y == -1) return ExistPiece(0, 1, 2, 3);
					//E
					if (p.x == 0 && p.y == -1) return ExistPiece(0, 1, 2, 3) || ExistPiece(2, 3, 2, 3);
					//F
					if (p.x == 1 && p.y == -1) return ExistPiece(2, 3, 2, 3);
					break;

				case Direction.Down:
					//A
					if (p.x == -1 && p.y == 2) return ExistPiece(0, 1, 2, 3);
					//B
					if (p.x == 0 && p.y == 2) return ExistPiece(0, 1, 2, 3) || ExistPiece(2, 3, 2, 3);
					//C
					if (p.x == 1 && p.y == 2) return ExistPiece(2, 3, 2, 3);
					//D
					if (p.x == -1 && p.y == 1) return ExistPiece(0, 1, 0, 1);
					//E
					if (p.x == 0 && p.y == 1) return ExistPiece(0, 1, 0, 1) || ExistPiece(2, 3, 0, 1);
					//F
					if (p.x == 1 && p.y == 1) return ExistPiece(2, 3, 0, 1);
					break;
			}
			throw new System.Exception();


			bool ExistPiece(byte minX, byte maxX, byte minY, byte maxY)
			{
				for (byte x = minX; x <= maxX; ++x)
					for (byte y = minY; y <= maxY; ++y)
						if (particles[x, y]) return true;
				return false;
			}
		}


		public override Bullet.CollisionResult OnCollision(Bullet bullet)
		{
			var p = bullet.index - index.TerrainArray_To_BulletArray();
			var result =

				// A
				(p.x == -1 && p.y == 1) ?
					(
						(bullet.direction == Direction.Right) ? CheckShortRange(firstRange: (0, 0, 2, 3), secondRange: (1, 1, 2, 3)) :
						(bullet.direction == Direction.Down) ? CheckShortRange(firstRange: (0, 1, 3, 3), secondRange: (0, 1, 2, 2)) :
						default
					) :

				// B
				(p.x == 0 && p.y == 1) ?
					(
						(bullet.direction == Direction.Right) ? CheckShortRange(firstRange: (2, 2, 2, 3), secondRange: (3, 3, 2, 3)) :
						(bullet.direction == Direction.Down) ? CheckLongRange(firstRange: (0, 3, 3, 3), secondRange: (0, 3, 2, 2)) :
						(bullet.direction == Direction.Left) ? CheckShortRange(firstRange: (1, 1, 2, 3), secondRange: (0, 0, 2, 3)) :
						default
					) :

				// C
				(p.x == 1 && p.y == 1) ?
					(
						(bullet.direction == Direction.Down) ? CheckShortRange(firstRange: (2, 3, 3, 3), secondRange: (2, 3, 2, 2)) :
						(bullet.direction == Direction.Left) ? CheckShortRange(firstRange: (3, 3, 2, 3), secondRange: (2, 2, 2, 3)) :
						default
					) :

				// D
				(p.x == -1 && p.y == 0) ?
					(
						(bullet.direction == Direction.Right) ? CheckLongRange(firstRange: (0, 0, 0, 3), secondRange: (1, 1, 0, 3)) :
						(bullet.direction == Direction.Down) ? CheckShortRange(firstRange: (0, 1, 1, 1), secondRange: (0, 1, 0, 0)) :
						(bullet.direction == Direction.Up) ? CheckShortRange(firstRange: (0, 1, 2, 2), secondRange: (0, 1, 3, 3)) :
						default
					) :

				// E
				(p.x == 0 && p.y == 0) ?
					(
						(bullet.direction == Direction.Right) ? CheckLongRange(firstRange: (2, 2, 0, 3), secondRange: (3, 3, 0, 3)) :
						(bullet.direction == Direction.Down) ? CheckLongRange(firstRange: (0, 3, 1, 1), secondRange: (0, 3, 0, 0)) :
						(bullet.direction == Direction.Left) ? CheckLongRange(firstRange: (1, 1, 0, 3), secondRange: (0, 0, 0, 3)) :
						CheckLongRange(firstRange: (0, 3, 2, 2), secondRange: (0, 3, 3, 3))
					) :

				// F
				(p.x == 1 && p.y == 0) ?
					(
						(bullet.direction == Direction.Down) ? CheckShortRange(firstRange: (2, 3, 1, 1), secondRange: (2, 3, 0, 0)) :
						(bullet.direction == Direction.Left) ? CheckLongRange(firstRange: (3, 3, 0, 3), secondRange: (2, 2, 0, 3)) :
						(bullet.direction == Direction.Up) ? CheckShortRange(firstRange: (2, 3, 2, 2), secondRange: (2, 3, 3, 3)) :
						default
					) :

				// G
				(p.x == -1 && p.y == -1) ?
					(
						(bullet.direction == Direction.Right) ? CheckShortRange(firstRange: (0, 0, 0, 1), secondRange: (1, 1, 0, 1)) :
						(bullet.direction == Direction.Up) ? CheckShortRange(firstRange: (0, 1, 0, 0), secondRange: (0, 1, 1, 1)) :
						default
					) :

				// H
				(p.x == 0 && p.y == -1) ?
					(
						(bullet.direction == Direction.Right) ? CheckShortRange(firstRange: (2, 2, 0, 1), secondRange: (3, 3, 0, 1)) :
						(bullet.direction == Direction.Left) ? CheckShortRange(firstRange: (1, 1, 0, 1), secondRange: (0, 0, 0, 1)) :
						(bullet.direction == Direction.Up) ? CheckLongRange(firstRange: (0, 3, 0, 0), secondRange: (0, 3, 1, 1)) :
						default
					) :

				// I
				(p.x == 1 && p.y == -1) ?
					(
						(bullet.direction == Direction.Left) ? CheckShortRange(firstRange: (3, 3, 0, 1), secondRange: (2, 2, 0, 1)) :
						(bullet.direction == Direction.Up) ? CheckShortRange(firstRange: (2, 3, 0, 0), secondRange: (2, 3, 1, 1)) :
						default
					) : default;


			if (result == Bullet.CollisionResult.RemoveBullet)
			{
				if (particles.isEmpty) Destroy(gameObject);
				Effect.Play(Effect.Anim.SmallExplosion, bullet.transform.position);
				if (bullet.allyType != Bullet.AllyType.Enemy || bullet.canDestroySteel) Effect.Play(Effect.Sound.Bullet_Collise_Brick);
			}
			return result;


			Bullet.CollisionResult CheckShortRange((byte minX, byte maxX, byte minY, byte maxY) firstRange, (byte minX, byte maxX, byte minY, byte maxY) secondRange)
			{
				bool firstRange_HasItem = CheckRange(firstRange);

				if (!bullet.canDestroySteel && firstRange_HasItem) return Bullet.CollisionResult.RemoveBullet;

				return CheckRange(secondRange) || firstRange_HasItem ? Bullet.CollisionResult.RemoveBullet : Bullet.CollisionResult.KeepBullet;


				bool CheckRange((byte minX, byte maxX, byte minY, byte maxY) range)
				{
					bool hasItem = false;
					for (byte x = range.minX; x <= range.maxX; ++x)
						for (byte y = range.minY; y <= range.maxY; ++y)
							if (particles[x, y])
							{
								hasItem = true;
								Destroy(particles[x, y]);
								particles[x, y] = null;
							}
					return hasItem;
				}
			}


			Bullet.CollisionResult CheckLongRange((byte minX, byte maxX, byte minY, byte maxY) firstRange, (byte minX, byte maxX, byte minY, byte maxY) secondRange)
			{
				var array = new (byte x, byte y)[4];
				byte x, y, i = 0;
				bool firstRange_HasItem = false;

				#region Kiểm tra firstRange
				for (x = firstRange.minX; x <= firstRange.maxX; ++x)
					for (y = firstRange.minY; y <= firstRange.maxY; ++y) array[i++] = (x, y);

				if (CheckPair(array[1], array[0])) firstRange_HasItem = true;
				if (CheckPair(array[2], array[3])) firstRange_HasItem = true;
				#endregion

				if (!bullet.canDestroySteel && firstRange_HasItem) return Bullet.CollisionResult.RemoveBullet;
				bool secondRange_HasItem = false;

				#region Kiểm tra secondRange
				i = 0;
				for (x = secondRange.minX; x <= secondRange.maxX; ++x)
					for (y = secondRange.minY; y <= secondRange.maxY; ++y) array[i++] = (x, y);

				if (CheckPair(array[1], array[0])) secondRange_HasItem = true;
				if (CheckPair(array[2], array[3])) secondRange_HasItem = true;
				#endregion

				return secondRange_HasItem || firstRange_HasItem ? Bullet.CollisionResult.RemoveBullet : Bullet.CollisionResult.KeepBullet;


				bool CheckPair((byte x, byte y) a, (byte x, byte y) b)
				{
					if (particles[a.x, a.y])
					{
						Destroy(particles[a.x, a.y]);
						particles[a.x, a.y] = null;
						if (particles[b.x, b.y])
						{
							Destroy(particles[b.x, b.y]);
							particles[b.x, b.y] = null;
						}
						return true;
					}
					return false;
				}
			}
		}
	}
}