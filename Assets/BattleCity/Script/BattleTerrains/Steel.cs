﻿using BattleCity.Tanks;
using UnityEngine;
using System;
using System.Collections.Generic;


namespace BattleCity.BattleTerrains
{
	public sealed class Steel : BattleTerrain
	{
		[SerializeField] private SerializedArray2D particles;


		private void Awake()
		{
			switch (name)
			{
				case Name.Steel_Full: break;

				case Name.Steel_Left: particles.DeleteItems(1, 1, 0, 1); break;
				case Name.Steel_Right: particles.DeleteItems(0, 0, 0, 1); break;
				case Name.Steel_Up: particles.DeleteItems(0, 1, 0, 0); break;
				case Name.Steel_Down: particles.DeleteItems(0, 1, 1, 1); break;

				case Name.Steel_Left_Up:
					particles.DeleteItems(0, 1, 0, 0);
					Destroy(particles[1, 1]);
					particles[1, 1] = null;
					break;

				case Name.Steel_Right_Up:
					particles.DeleteItems(0, 1, 0, 0);
					Destroy(particles[0, 1]);
					particles[0, 1] = null;
					break;

				case Name.Steel_Right_Down:
					particles.DeleteItems(0, 1, 1, 1);
					Destroy(particles[0, 0]);
					particles[0, 0] = null;
					break;

				case Name.Steel_Left_Down:
					particles.DeleteItems(0, 1, 1, 1);
					Destroy(particles[1, 0]);
					particles[1, 0] = null;
					break;
			}
		}


		public override bool IsBlockingTankMove(Vector3Int tankIndex, Direction tankDirecton, bool tankHasShip)
		{
			var p = tankIndex - index.TerrainArray_To_TankArray();
			switch (tankDirecton)
			{
				case Direction.Right:
					// A
					if (p.x == -2 && p.y == 1) return particles[0, 1];
					// B
					if (p.x == -2 && p.y == 0) return particles[0, 1] || particles[0, 0];
					// C
					if (p.x == -2 && p.y == -1) return particles[0, 0];
					// D
					if (p.x == -1 && p.y == 1) return particles[1, 1];
					// E
					if (p.x == -1 && p.y == 0) return particles[1, 1] || particles[1, 0];
					// F
					if (p.x == -1 && p.y == -1) return particles[1, 0];
					break;

				case Direction.Left:
					// A
					if (p.x == 2 && p.y == 1) return particles[1, 1];
					// B
					if (p.x == 2 && p.y == 0) return particles[1, 1] || particles[1, 0];
					// C
					if (p.x == 2 && p.y == -1) return particles[1, 0];
					// D
					if (p.x == 1 && p.y == 1) return particles[0, 1];
					// E
					if (p.x == 1 && p.y == 0) return particles[0, 1] || particles[0, 0];
					// F
					if (p.x == 1 && p.y == -1) return particles[0, 0];
					break;

				case Direction.Up:
					// A
					if (p.x == -1 && p.y == -2) return particles[0, 0];
					// B
					if (p.x == 0 && p.y == -2) return particles[0, 0] || particles[1, 0];
					// C
					if (p.x == 1 && p.y == -2) return particles[1, 0];
					// D
					if (p.x == -1 && p.y == -1) return particles[0, 1];
					//E
					if (p.x == 0 && p.y == -1) return particles[0, 1] || particles[1, 1];
					// F
					if (p.x == 1 && p.y == -1) return particles[1, 1];
					break;

				case Direction.Down:
					// A
					if (p.x == -1 && p.y == 2) return particles[0, 1];
					// B
					if (p.x == 0 && p.y == 2) return particles[0, 1] || particles[1, 1];
					// C
					if (p.x == 1 && p.y == 2) return particles[1, 1];
					// D
					if (p.x == -1 && p.y == 1) return particles[0, 0];
					// E
					if (p.x == 0 && p.y == 1) return particles[0, 0] || particles[1, 0];
					// F
					if (p.x == 1 && p.y == 1) return particles[1, 0];
					break;
			}
			throw new Exception();
		}


		public override Bullet.CollisionResult OnCollision(Bullet bullet)
		{
			var pieces = Steel_Forest_OnCollision_Bullet(particles, bullet.index - index.TerrainArray_To_BulletArray());
			if (pieces.Count == 0) return Bullet.CollisionResult.KeepBullet;

			if (bullet.canDestroySteel)
			{
				foreach (var (x, y) in pieces)
				{
					Destroy(particles[x, y]);
					particles[x, y] = null;
				}
				if (particles.isEmpty) Destroy(gameObject);
			}

			Effect.Play(Effect.Anim.SmallExplosion, bullet.transform.position);
			if (bullet.canDestroySteel) Effect.Play(Effect.Sound.Bullet_Collise_Brick);
			else if (bullet.allyType != Bullet.AllyType.Enemy) Effect.Play(Effect.Sound.Bullet_Collise_Wall);
			return Bullet.CollisionResult.RemoveBullet;
		}



		///<summary>
		///Return: danh sách các piece có va chạm với bullet
		/// </summary>
		/// <param name="p"> p = bullet index - (terrain To Bullet index) </param>
		internal static IReadOnlyList<(byte x, byte y)> Steel_Forest_OnCollision_Bullet(SerializedArray2D particles, Vector3Int p)
		{
			var pieces = new List<(byte x, byte y)>(4);

			// A
			if (p.x == -1 && p.y == 1)
			{
				if (particles[0, 1]) pieces.Add((0, 1));
			}

			// B
			else if (p.x == 0 && p.y == 1)
			{
				if (particles[0, 1]) pieces.Add((0, 1));
				if (particles[1, 1]) pieces.Add((1, 1));
			}

			// C
			else if (p.x == 1 && p.y == 1)
			{
				if (particles[1, 1]) pieces.Add((1, 1));
			}

			// D
			else if (p.x == -1 && p.y == 0)
			{
				if (particles[0, 0]) pieces.Add((0, 0));
				if (particles[0, 1]) pieces.Add((0, 1));
			}

			// E
			else if (p.x == 0 && p.y == 0)
			{
				for (byte x = 0; x < 2; ++x)
					for (byte y = 0; y < 2; ++y)
						if (particles[x, y]) pieces.Add((x, y));
			}

			// F
			else if (p.x == 1 && p.y == 0)
			{
				if (particles[1, 0]) pieces.Add((1, 0));
				if (particles[1, 1]) pieces.Add((1, 1));
			}

			// G
			else if (p.x == -1 && p.y == -1)
			{
				if (particles[0, 0]) pieces.Add((0, 0));
			}

			// H
			else if (p.x == 0 && p.y == -1)
			{
				if (particles[0, 0]) pieces.Add((0, 0));
				if (particles[1, 0]) pieces.Add((1, 0));
			}

			// I
			else if (p.x == 1 && p.y == -1)
			{
				if (particles[1, 0]) pieces.Add((1, 0));
			}

			return pieces;
		}
	}
}