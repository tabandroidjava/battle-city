﻿using BattleCity.Tanks;
using UnityEngine;


namespace BattleCity.BattleTerrains
{
	[RequireComponent(typeof(SpriteRenderer))]
	public sealed class Sand : BattleTerrain, ITankCollision
	{
		public override bool IsBlockingTankMove(Vector3Int tankIndex, Direction tankDirecton, bool tankHasShip) => false;


		public override Bullet.CollisionResult OnCollision(Bullet bullet) => Bullet.CollisionResult.KeepBullet;


		public void OnCollision(Tank tank)
		{

		}
	}
}