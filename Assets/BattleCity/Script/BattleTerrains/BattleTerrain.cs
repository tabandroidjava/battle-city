﻿using UnityEngine;
using BattleCity.Tanks;
using System;


namespace BattleCity.BattleTerrains
{
	[DisallowMultipleComponent]
	public abstract class BattleTerrain : MonoBehaviour, IBulletCollision
	{
		[Serializable]
		internal struct SerializedArray2D
		{
			[Serializable]
			private struct Row
			{
				public GameObject[] items;
			}
			[SerializeField] private Row[] rows;


			internal void DeleteItems(byte minX, byte maxX, byte minY, byte maxY)
			{
				byte x, y;
				for (x = minX; x <= maxX; ++x)
					for (y = minY; y <= maxY; ++y)
					{
						Destroy(rows[x].items[y]);
						rows[x].items[y] = null;
					}
			}


			internal bool isEmpty
			{
				get
				{
					foreach (var row in rows)
						foreach (var item in row.items) if (item) return false;
					return true;
				}
			}


			internal GameObject this[int x, int y]
			{
				get => rows[x].items[y];
				set => rows[x].items[y] = value;
			}
		}


		public enum Name : int
		{
			Null = 0,
			Brick_Left = 1,
			Brick_Left_Up = 2,
			Brick_Up = 3,
			Brick_Right_Up = 4,
			Brick_Right = 5,
			Brick_Right_Down = 6,
			Brick_Down = 7,
			Brick_Left_Down = 8,
			Brick_Full = 9,

			Steel_Left = 10,
			Steel_Left_Up = 11,
			Steel_Up = 12,
			Steel_Right_Up = 13,
			Steel_Right = 14,
			Steel_Right_Down = 15,
			Steel_Down = 16,
			Steel_Left_Down = 17,
			Steel_Full = 18,

			Water = 19,
			Sand = 20,
			Forest = 21,
			Eagle = 22
		}

		public static BattleTerrain[][] array;
		protected Vector3Int index { get; private set; }


		#region Tạo instance
		internal static void Init(int[][] map)
		{
			var size = (x: map.Length, y: map[0].Length);
			Extensions.InitArraysAndCoordinate(size);
			var index = new Vector3Int();
			for (index.x = 0; index.x < size.x; ++index.x)
				for (index.y = 0; index.y < size.y; ++index.y)
					Create((Name)map[index.x][index.y], index);
		}


		protected static new Name name;

		internal static BattleTerrain Create(Name name, Vector3Int index)
		{
			BattleTerrain prefab;
			int value = (int)name;
			switch (name)
			{
				case Name.Null: return null;
				case Name.Sand: prefab = Extensions.Load<Sand>(); break;
				case Name.Water: prefab = Extensions.Load<Water>(); break;
				case Name.Forest: prefab = Extensions.Load<Forest>(); break;
				case Name.Eagle: prefab = Extensions.Load<Eagle>(); break;

				default:
					prefab = ((int)Name.Brick_Left <= value && value <= (int)Name.Brick_Full) ? Extensions.Load<Brick>() : Extensions.Load<Steel>() as BattleTerrain;
					break;
			}

			BattleTerrain.name = name;
			var terrain = array[index.x][index.y] = Instantiate(prefab, index.TerrainArray_To_TerrainWorld(), Quaternion.identity);
			terrain.transform.parent = BattleField.instance.terrainAnchor;
			terrain.index = index;
			return terrain;
		}
		#endregion


		protected void OnDisable() => array[index.x][index.y] = null;


		public abstract Bullet.CollisionResult OnCollision(Bullet bullet);

		/// <summary>
		/// Cản không cho <see cref="Tank"/> di chuyển 1 bước ?
		/// </summary>
		/// <param name="tankHasShip">Có thuyền hay không ? (<see cref="Tank.ship"/> != <see langword="null"/> ?)</param>
		/// <returns></returns>
		public abstract bool IsBlockingTankMove(Vector3Int tankIndex, Direction tankDirecton, bool tankHasShip);
	}
}