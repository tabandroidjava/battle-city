﻿using BattleCity.Tanks;
using UnityEngine;


namespace BattleCity.BattleTerrains
{
	[RequireComponent(typeof(SpriteRenderer), typeof(Animator))]
	public sealed class Water : BattleTerrain
	{
		public override Bullet.CollisionResult OnCollision(Bullet bullet) => Bullet.CollisionResult.KeepBullet;


		public override bool IsBlockingTankMove(Vector3Int tankIndex, Direction tankDirecton, bool tankHasShip) => !tankHasShip;
	}
}