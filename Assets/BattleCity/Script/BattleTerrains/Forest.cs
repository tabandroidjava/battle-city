﻿using BattleCity.Tanks;
using UnityEngine;


namespace BattleCity.BattleTerrains
{
	public sealed class Forest : BattleTerrain
	{
		[SerializeField] private SerializedArray2D particles;


		public override bool IsBlockingTankMove(Vector3Int tankIndex, Direction tankDirecton, bool tankHasShip) => false;


		public override Bullet.CollisionResult OnCollision(Bullet bullet)
		{
			var pieces = Steel.Steel_Forest_OnCollision_Bullet(particles, bullet.index - index.TerrainArray_To_BulletArray());
			if (pieces.Count == 0) return Bullet.CollisionResult.KeepBullet;

			if (bullet.canBurnForest)
			{
				foreach (var (x, y) in pieces)
				{
					Destroy(particles[x, y]);
					particles[x, y] = null;
				}
				if (particles.isEmpty) Destroy(gameObject);
			}
			return Bullet.CollisionResult.KeepBullet;
		}
	}
}