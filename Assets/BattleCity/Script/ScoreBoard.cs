﻿using UnityEngine;
using System.Collections.Generic;
using BattleCity.Tanks;
using UnityEngine.UI;
using sd = RotaryHeart.Lib.SerializableDictionary;
using System;
using UniRx.Async;
using UnityEngine.SceneManagement;


namespace BattleCity
{
	public sealed class ScoreBoard : MonoBehaviour
	{
		private static ScoreBoard instance;
		private static bool overFlowException;


		#region Non-UI Fields
		#region highScore
		private const string HIGH_SCORE_KEY = "ScoreBoard:0";
		private static int _highScore;
		public static int highScore
		{
			get => _highScore;
			private set
			{
				_highScore = value;
				if (instance) instance.highScoreText.text = $"Hi: {value}";
				PlayerPrefs.SetInt(HIGH_SCORE_KEY, value);
				PlayerPrefs.Save();
			}
		}


		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void Init()
		{
			_highScore = PlayerPrefs.GetInt(HIGH_SCORE_KEY);
		}
		#endregion


		#region scores
		private static readonly IReadOnlyDictionary<Enemy.Type, int> ENEMY_BONUS_SCORES = new Dictionary<Enemy.Type, int>()
		{
			[Enemy.Type.Small] = 100,
			[Enemy.Type.Fast] = 200,
			[Enemy.Type.Big] = 300,
			[Enemy.Type.Armored] = 400
		};
		private static readonly Dictionary<Tank.Color, int> scores = new Dictionary<Tank.Color, int>()
		{
			[Tank.Color.Yellow] = 0,
			[Tank.Color.Green] = 0
		};
		private const int SCORE_RANGE_TO_INCREASE_LIFE = 10000;
		private static readonly Dictionary<Tank.Color, int> scoresToIncreaseLife = new Dictionary<Tank.Color, int>()
		{
			[Tank.Color.Yellow] = SCORE_RANGE_TO_INCREASE_LIFE,
			[Tank.Color.Green] = SCORE_RANGE_TO_INCREASE_LIFE
		};
		#endregion


		public static readonly IReadOnlyDictionary<Tank.Color, Dictionary<Enemy.Type, byte>> killedEnemies = new Dictionary<Tank.Color, Dictionary<Enemy.Type, byte>>()
		{
			[Tank.Color.Yellow] = new Dictionary<Enemy.Type, byte>()
			{
				[Enemy.Type.Small] = 0,
				[Enemy.Type.Fast] = 0,
				[Enemy.Type.Big] = 0,
				[Enemy.Type.Armored] = 0
			},
			[Tank.Color.Green] = new Dictionary<Enemy.Type, byte>()
			{
				[Enemy.Type.Small] = 0,
				[Enemy.Type.Fast] = 0,
				[Enemy.Type.Big] = 0,
				[Enemy.Type.Armored] = 0
			},
		};
		#endregion


		#region UI Fields
		[SerializeField] private Text stageText, highScoreText;
		[Serializable] private sealed class EnemyType_Text_Dict : sd.SerializableDictionaryBase<Enemy.Type, Text> { }
		[Serializable] private sealed class TankColor_EnemyType_Text_Dict : sd.SerializableDictionaryBase<Tank.Color, EnemyType_Text_Dict> { }
		[SerializeField] private TankColor_EnemyType_Text_Dict killedEnemyTexts;

		[Serializable]
		private sealed class PlayerScoreUI
		{
			public const int KILL_MANY_BONUS = 1000;
			public Text oldScore, newScore, bonusScore;
		}
		[Serializable] private sealed class TankColor_PlayerScoreUI_Dict : sd.SerializableDictionaryBase<Tank.Color, PlayerScoreUI> { }
		[SerializeField] private TankColor_PlayerScoreUI_Dict playerScoreUIs;
		#endregion


		[SerializeField] private int delayMiliseconds = 5000;

		private async void Awake()
		{
			if (instance) throw new Exception();
			instance = this;
			stageText.text = $"Stage: {BattleField.stage}";
			highScoreText.text = $"Hi: {highScore}";

			#region Cập nhật killedEnemyTexts
			foreach (var color_typeCount in killedEnemies)
				foreach (var type_count in color_typeCount.Value)
					killedEnemyTexts[color_typeCount.Key][type_count.Key].text = type_count.Value.ToString();
			#endregion

			#region Cập nhật playerScoreUIs
			foreach (var color_ui in playerScoreUIs)
			{
				var color = color_ui.Key;
				var ui = color_ui.Value;
				ui.oldScore.text = scores[color].ToString();

				int count = 0;
				foreach (var type_count in killedEnemies[color])
				{
					count += type_count.Value;
					IncreaseScore(color, type_count.Value * ENEMY_BONUS_SCORES[type_count.Key]);
				}
				if (count > BattleField.battle.totalEnemy / 2)
				{
					ui.bonusScore.text = $"Bonus +{PlayerScoreUI.KILL_MANY_BONUS}";
					IncreaseScore(color, PlayerScoreUI.KILL_MANY_BONUS);
					Effect.Play(Effect.Sound.Scoreboard_Bonus);
				}
				ui.newScore.text = scores[color].ToString();
			}
			#endregion

			#region Cập nhật highScore
			int max = 0;
			foreach (int score in scores.Values) if (score > max) max = score;
			if (max > highScore) highScore = max;
			#endregion

			await UniTask.Delay(delayMiliseconds);
			if (overFlowException || BattleField.stage == int.MaxValue)
			{
				// Hiển thị Animation HIGH SCORE , SCORE ĐẠT MAX

				overFlowException = false;
				highScore = 0;
				foreach (var color in Player.PLAYER_COLORS) scores[color] = 0;
				SceneManager.LoadScene("Main");
			}
			else if (BattleField.isGameOver)
			{
				foreach (var color in Player.PLAYER_COLORS) scores[color] = 0;
				SceneManager.LoadScene("Main");
			}
			else SceneManager.LoadScene("BattleField");
		}


		private void OnDisable()
		{
			instance = null;
			foreach (var color_typeCount in killedEnemies)
			{
				var type_count = color_typeCount.Value;
				foreach (Enemy.Type type in Enum.GetValues(typeof(Enemy.Type))) type_count[type] = 0;
			}
		}


		public static void IncreaseScore(Tank.Color color, int delta)
		{
			try
			{
				checked
				{
					scores[color] += delta;
				}
			}
			catch (OverflowException)
			{
				overFlowException = true;
				scores[color] = int.MaxValue;
			}

			if (scores[color] >= scoresToIncreaseLife[color])
			{
				try
				{
					checked
					{
						scoresToIncreaseLife[color] = scores[color] + SCORE_RANGE_TO_INCREASE_LIFE;
					}
				}
				catch (OverflowException)
				{
					scoresToIncreaseLife[color] = int.MaxValue;
				}

				++Player.lifes[color];
				Effect.Play(Effect.Sound.Life);
			}
		}
	}
}