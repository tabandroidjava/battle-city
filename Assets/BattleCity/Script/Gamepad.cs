﻿using UnityEngine;
using System;
using System.Collections.Generic;
using BattleCity.Util;


namespace BattleCity
{
	public interface IGamepadListener
	{
		void OnDpad(Direction direction, Gamepad.ButtonState state);

		void OnButtonA(Gamepad.ButtonState state);

		void OnButtonB(Gamepad.ButtonState state);

		void OnButtonX(Gamepad.ButtonState state);

		void OnButtonY(Gamepad.ButtonState state);

		void OnButtonStart();

		void OnButtonSelect();

		Gamepad.LocalState state { get; }
	}



	[DisallowMultipleComponent]
	public sealed class Gamepad : MonoBehaviour
	{
		#region KHỞI TẠO
		public static readonly IReadOnlyDictionary<int, Gamepad> instances = new Dictionary<int, Gamepad>();

		/// <summary>
		/// [0, 1]
		/// </summary>
		public int ID { get; private set; }


		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void Init()
		{
			(instances as Dictionary<int, Gamepad>)[0] = Instantiate(Extensions.Load<Gamepad>());
			instances[0].name = "Gamepad 0";
			(instances as Dictionary<int, Gamepad>)[1] = Instantiate(Extensions.Load<Gamepad>());
			instances[1].name = "Gamepad 1";
		}


		private void Awake()
		{
			if (instances.Count == 2) throw new Exception("Không thể tạo Gamepad, chỉ cho phép duy nhất 2 Gamepad !");
			DontDestroyOnLoad(this);
			var components = GetComponents<GamepadComponent>();
			if (components.Length == 0) throw new Exception("Cần ít nhất 1 GamepadComponent !");
			ID = instances.Count;
			foreach (var c in components)
			{
				if (c.enabled) throw new Exception("Kéo GamepadComponent vào gameObject và TẮT !");
				c.enabled = true;
			}
		}
		#endregion


		#region PUBLIC INPUT EVENTS/ STATES
		public enum ButtonState
		{
			PRESS, HOLD, RELEASE
		}

		public sealed class LocalState
		{
			public readonly IReadOnlyDictionary<Direction, Queue<ButtonState>> dpad = new Dictionary<Direction, Queue<ButtonState>>()
			{
				[Direction.Left] = new Queue<ButtonState>(),
				[Direction.Up] = new Queue<ButtonState>(),
				[Direction.Right] = new Queue<ButtonState>(),
				[Direction.Down] = new Queue<ButtonState>()
			};
			public readonly IReadOnlyDictionary<Button, Queue<ButtonState>> buttons = new Dictionary<Button, Queue<ButtonState>>()
			{
				[Button.A] = new Queue<ButtonState>(),
				[Button.B] = new Queue<ButtonState>(),
				[Button.X] = new Queue<ButtonState>(),
				[Button.Y] = new Queue<ButtonState>()
			};
			public int buttonStart, buttonSelect;
		}

		public event Action<Direction, ButtonState> onDpad;
		public event Action<ButtonState> onButtonA, onButtonB, onButtonX, onButtonY;
		public event Action onButtonStart, onButtonSelect;


		#region Xử lý listener
		private readonly List<IGamepadListener> listeners = new List<IGamepadListener>();
		private readonly List<IGamepadListener> itemsToAdd = new List<IGamepadListener>();

		public void AddListener(IGamepadListener listener)
		{
			if (!itemsToAdd.Contains(listener)) itemsToAdd.Add(listener);
			if (itemsToRemove.Contains(listener)) itemsToRemove.Remove(listener);
		}


		private readonly List<IGamepadListener> itemsToRemove = new List<IGamepadListener>();

		public void RemoveListener(IGamepadListener listener)
		{
			if (!itemsToRemove.Contains(listener)) itemsToRemove.Add(listener);
			if (itemsToAdd.Contains(listener)) itemsToAdd.Remove(listener);
		}


		public bool ContainListener(IGamepadListener listener) => listeners.Contains(listener);
		#endregion


		public enum Button
		{
			A, B, X, Y
		}

		private readonly Dictionary<Button, ButtonState> buttonStates = new Dictionary<Button, ButtonState>()
		{
			[Button.A] = ButtonState.RELEASE,
			[Button.B] = ButtonState.RELEASE,
			[Button.X] = ButtonState.RELEASE,
			[Button.Y] = ButtonState.RELEASE
		};

		public ButtonState GetButtonState(Button button) => buttonStates[button];

		public bool isStartPressed { get; private set; }
		public bool isSelectPressed { get; private set; }

		public ButtonState GetDpadState(Direction direction) => dpadStates[direction];

		private readonly Dictionary<Direction, ButtonState> dpadStates = new Dictionary<Direction, ButtonState>()
		{
			[Direction.Left] = ButtonState.RELEASE,
			[Direction.Up] = ButtonState.RELEASE,
			[Direction.Right] = ButtonState.RELEASE,
			[Direction.Down] = ButtonState.RELEASE
		};
		#endregion


		public void Vibrate(float speed = 1, float delaySeconds = 1)
		{
			var c = GamepadComponent.currents[this];
			if (c) c.Vibrate(speed, delaySeconds);
		}


		#region Cập nhật listeners và reset isStartPressed, isSelectPressed
		private void Update()
		{
			if (itemsToAdd.Count != 0)
			{
				foreach (var item in itemsToAdd) if (!listeners.Contains(item)) listeners.Add(item);
				itemsToAdd.Clear();
			}
			if (itemsToRemove.Count != 0)
			{
				foreach (var item in itemsToRemove) if (listeners.Contains(item)) listeners.Remove(item);
				itemsToRemove.Clear();
			}
		}


		private void LateUpdate()
		{
			isStartPressed = isSelectPressed = false;
		}
		#endregion


		[SerializeField] private float holdingDelaySeconds = 0.05f;


		#region Dpad
		private readonly Dictionary<Direction, bool> holdingDpad = new Dictionary<Direction, bool>()
		{
			[Direction.Left] = false,
			[Direction.Up] = false,
			[Direction.Right] = false,
			[Direction.Down] = false
		};
		private readonly Dictionary<Direction, float> dpadStopTime = new Dictionary<Direction, float>();


		internal void __internal__UpdateDpad(ButtonState logicalState, Direction direction)
		{
			void InvokeEvent()
			{
				dpadStates[direction] = logicalState;
				foreach (var listener in listeners)
					if (!itemsToRemove.Contains(listener))
					{
						listener.state.dpad[direction].SafeEnqueue(logicalState);
						listener.OnDpad(direction, logicalState);
					}
				onDpad?.Invoke(direction, logicalState);
			}

			switch (logicalState)
			{
				case ButtonState.PRESS:
					dpadStopTime[direction] = Time.time + holdingDelaySeconds;
					InvokeEvent();
					break;

				case ButtonState.HOLD:
					if (Time.time >= dpadStopTime[direction])
					{
						holdingDpad[direction] = true;
						InvokeEvent();
					}
					break;

				case ButtonState.RELEASE:
					holdingDpad[direction] = false;
					InvokeEvent();
					break;
			}
		}
		#endregion


		#region A, B, X, Y
		private readonly Dictionary<Button, bool> holdingButton = new Dictionary<Button, bool>()
		{
			[Button.A] = false,
			[Button.B] = false,
			[Button.X] = false,
			[Button.Y] = false
		};
		private readonly Dictionary<Button, float> buttonStopTime = new Dictionary<Button, float>()
		{
			[Button.A] = 0,
			[Button.B] = 0,
			[Button.X] = 0,
			[Button.Y] = 0
		};


		internal void __internal__UpdateABXY(ButtonState logicalState, Button button)
		{
			void InvokeEvent()
			{
				buttonStates[button] = logicalState;
				foreach (var listener in listeners)
					if (!itemsToRemove.Contains(listener))
					{
						listener.state.buttons[button].SafeEnqueue(logicalState);
						switch (button)
						{
							case Button.A: listener.OnButtonA(logicalState); break;
							case Button.B: listener.OnButtonB(logicalState); break;
							case Button.X: listener.OnButtonX(logicalState); break;
							case Button.Y: listener.OnButtonY(logicalState); break;
						}
					}
				switch (button)
				{
					case Button.A: onButtonA?.Invoke(logicalState); break;
					case Button.B: onButtonB?.Invoke(logicalState); break;
					case Button.X: onButtonX?.Invoke(logicalState); break;
					case Button.Y: onButtonY?.Invoke(logicalState); break;
				}
			}

			switch (logicalState)
			{
				case ButtonState.PRESS:
					buttonStopTime[button] = Time.time + holdingDelaySeconds;
					InvokeEvent();
					break;

				case ButtonState.HOLD:
					if (Time.time >= buttonStopTime[button])
					{
						holdingButton[button] = true;
						InvokeEvent();
					}
					break;

				case ButtonState.RELEASE:
					holdingButton[button] = false;
					InvokeEvent();
					break;
			}
		}
		#endregion


		#region Start, Select
		internal void __internal__PressStart()
		{
			isStartPressed = true;
			foreach (var listener in listeners)
				if (!itemsToRemove.Contains(listener))
				{
					if (listener.state.buttonStart == int.MaxValue) listener.state.buttonStart = 0; else ++listener.state.buttonStart;
					listener.OnButtonStart();
				}
			onButtonStart?.Invoke();
		}


		internal void __internal__PressSelect()
		{
			isSelectPressed = true;
			foreach (var listener in listeners)
				if (!itemsToRemove.Contains(listener))
				{
					if (listener.state.buttonSelect == int.MaxValue) listener.state.buttonSelect = 0; else ++listener.state.buttonSelect;
					listener.OnButtonSelect();
				}
			onButtonSelect?.Invoke();
		}
		#endregion


		#region Reset
		private static readonly Direction[] ALL_DIRECTIONS = (Direction[])Enum.GetValues(typeof(Direction));
		private static readonly Button[] ALL_BUTTONS = (Button[])Enum.GetValues(typeof(Button));

		internal void __internal__Reset()
		{
			foreach (var dir in ALL_DIRECTIONS) if (GetDpadState(dir) != ButtonState.RELEASE) __internal__UpdateDpad(ButtonState.RELEASE, dir);
			foreach (var b in ALL_BUTTONS) if (GetButtonState(b) != ButtonState.RELEASE) __internal__UpdateABXY(ButtonState.RELEASE, b);
			isStartPressed = isSelectPressed = false;
		}
		#endregion
	}



	internal static class GamepadExtension
	{
		private const int MAX_QUEUE_COUNT = 1000;


		public static void SafeEnqueue<T>(this Queue<T> queue, T item)
		{
			while (queue.Count >= MAX_QUEUE_COUNT) queue.Dequeue();
			queue.Enqueue(item);
		}
	}
}