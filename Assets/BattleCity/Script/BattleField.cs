﻿using UnityEngine;
using BattleCity.Tanks;
using UniRx.Async;
using System.Threading;
using System;
using System.Collections.Generic;
using BattleCity.BattleTerrains;
using UnityEngine.SceneManagement;
using BattleCity.AI;
using UnityEngine.UI;
using sd = RotaryHeart.Lib.SerializableDictionary;


namespace BattleCity
{
	public sealed class BattleField : MonoBehaviour
	{
		public static BattleField instance { get; private set; }
		public Transform itemAnchor, terrainAnchor, bulletAnchor, noShipEnemyAnchor;
		private static CancellationTokenSource cancelSource;

		/// <summary>
		/// Khi BattleField bị disable thì <code><see cref="Token"/>.IsCancellationRequested == <see langword="true"/></code>
		/// </summary>
		/// <exception cref="ObjectDisposedException">BattleField đã disable</exception>
		public static CancellationToken Token => cancelSource.Token;


		#region Cập nhật/reset stage và campaignEnum
		public static int stage { get; private set; } = 1;
		private static IEnumerator<Campaign.Battle> campaignEnum;
		public static Campaign.Battle battle => campaignEnum.Current;


		private void Awake()
		{
			if (instance) throw new Exception();
			instance = this;
			if (isGameOver)
			{
				isGameOver = false;
				stage = 1;
				campaignEnum.Dispose();
				campaignEnum = Setting.current.campaign.GetEnumerator();
			}
			else if (stage == int.MaxValue)
			{
				stage = 1;
				campaignEnum.Dispose();
				campaignEnum = Setting.current.campaign.GetEnumerator();
			}
			else if (campaignEnum == null) campaignEnum = Setting.current.campaign.GetEnumerator();
			else ++stage;

			stageText.text = stage.ToString();
			campaignEnum.MoveNext();
			BattleTerrain.Init(campaignEnum.Current.map);
			ModifyCameraAndScaleUI();
		}
		#endregion


		private void Start()
		{
			EnemyAgent.Init();
			enemyLifeUI.Update(Enemy.lifes.totalLife);
			foreach (var color_life in Player.lifes) playerLifeTexts[color_life.Key].text = color_life.Value.ToString();
			foreach (var color in Player.PLAYER_COLORS) if (Player.players[color] || Player.lifes[color] != 0) Player.Spawn(color, 0).CheckException();
			Effect.Play(Effect.Sound.Start_BattleField);
		}


		private void OnEnable()
		{
			cancelSource = new CancellationTokenSource();
			Gamepad.instances[0].onButtonA += OnButtonA_0;
			Gamepad.instances[1].onButtonA += OnButtonA_1;
		}


		private void OnDisable()
		{
			instance = null;
			cancelSource.Cancel();
			cancelSource.Dispose();
			Gamepad.instances[0].onButtonA -= OnButtonA_0;
			Gamepad.instances[1].onButtonA -= OnButtonA_1;
			while (Enemy.enemies.Count != 0) Enemy.enemies[0].gameObject.SetActive(false);

			foreach (var color in Player.PLAYER_COLORS)
			{
				var player = Player.players[color];
				if (player && player.isActiveAndEnabled) player.gameObject.SetActive(false);
			}
		}


		#region Event: Nếu player do Gamepad điều khiển thì xử lý mượn mạng từ đồng đội
		private async void OnButtonA_0(Gamepad.ButtonState state)
		{

			var color = Tank.Color.Yellow;
			if (state != Gamepad.ButtonState.PRESS || Player.players[color] || PlayerAgent.agents[color] || Player.lifes[color] != 0 || Player.lifes[Tank.Color.Green] == 0) return;
			await Player.Spawn(color, 1000, borrowLife: true);
		}


		private async void OnButtonA_1(Gamepad.ButtonState state)
		{
			var color = Tank.Color.Green;
			if (state != Gamepad.ButtonState.PRESS || Player.players[color] || PlayerAgent.agents[color] || Player.lifes[color] != 0 || Player.lifes[Tank.Color.Yellow] == 0) return;
			await Player.Spawn(color, 1000, borrowLife: true);
		}
		#endregion


		#region Xử lý GameOver và Win
		public static bool isGameOver { get; private set; }
		private static CancellationTokenSource cancelFinish = new CancellationTokenSource();

		public static async UniTask Finish(bool isGameOver)
		{
			if (BattleField.isGameOver) throw new Exception("Player đã thua (GameOver). Không thể xử lý thêm bất kỳ sự kiện WIN hay GAMEOVER nào nữa!");
			if (isGameOver)
			{
				cancelFinish?.Cancel();
				cancelFinish?.Dispose();
				cancelFinish = null;
				foreach (var player in Player.players.Values) if (player) player.Freeze(true);
				BattleField.isGameOver = true;
				await UniTask.Delay(5000);
			}
			else
			{
				cancelFinish = new CancellationTokenSource();
				if (await UniTask.Delay(5000, cancellationToken: cancelFinish.Token).SuppressCancellationThrow()) return;
				cancelFinish.Dispose();
				cancelFinish = null;
			}
			SceneManager.LoadScene("ScoreBoard");
		}
		#endregion


		#region UI
		[SerializeField] private new Camera camera;
		[SerializeField] private RectTransform ui;


		private void ModifyCameraAndScaleUI()
		{
			float aspect = camera.aspect = BattleTerrain.array.Length / (float)BattleTerrain.array[0].Length;
			camera.orthographicSize = BattleTerrain.array[0].Length / 2f;
			var pxRect = camera.pixelRect;
			float boundAspect = pxRect.width / pxRect.height;
			var c = pxRect.center;
			if (boundAspect >= aspect) pxRect.width = pxRect.height * aspect;
			else pxRect.height = pxRect.width / aspect;
			pxRect.center = c;
			camera.pixelRect = pxRect;
			ui.anchorMin = new Vector2(camera.rect.xMax, 0);
		}


		[SerializeField] private Text stageText;
		[Serializable] public sealed class TankColor_Text_Dict : sd.SerializableDictionaryBase<Tank.Color, Text> { }
		public TankColor_Text_Dict playerLifeTexts;

		[Serializable]
		public sealed class EnemyLifeUI
		{
			[SerializeField] private Image[] icons;
			[SerializeField] private Text overLimitText;
			private int index = -1;


			public void Update(int totalLife)
			{
				if (totalLife < 0) throw new ArgumentOutOfRangeException("totalLife không được < 0 !");

				int stop;
				if (totalLife > icons.Length)
				{
					overLimitText.text = $"+{totalLife - icons.Length}";
					stop = icons.Length - 1;
				}
				else
				{
					overLimitText.text = "";
					stop = totalLife - 1;
				}

				if (index > stop)
					do
					{
						icons[index].gameObject.SetActive(false);
						--index;
					} while (index != stop);
				else if (index < stop)
					do
					{
						++index;
						icons[index].gameObject.SetActive(true);
					} while (index != stop);
			}
		}
		public EnemyLifeUI enemyLifeUI;
		#endregion
	}
}