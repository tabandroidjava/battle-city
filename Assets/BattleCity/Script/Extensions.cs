﻿using UnityEngine;
using System.Collections.Generic;
using BattleCity.Tanks;
using BattleCity.BattleTerrains;
using System.Threading;
using UniRx.Async;
using System;
using System.Collections;


namespace BattleCity
{
	public static class Extensions
	{
		private static readonly Dictionary<string, UnityEngine.Object> cachePrefabs = new Dictionary<string, UnityEngine.Object>();

		public static T Load<T>(string postFix = "") where T : UnityEngine.Object
		{
			string key = $"{typeof(T)}{postFix}";
			return (cachePrefabs.ContainsKey(key) ? cachePrefabs[key] : (cachePrefabs[key] = Resources.Load<T>(key))) as T;
		}


		#region Chuyển đổi hệ tọa độ
		private static Vector3 mapWorldOrigin, bulletWorldOrigin;

		public static void InitArraysAndCoordinate((int x, int y) mapSize)
		{
			BattleTerrain.array = new BattleTerrain[mapSize.x][];
			for (int x = 0; x < mapSize.x; ++x) BattleTerrain.array[x] = new BattleTerrain[mapSize.y];
			mapWorldOrigin = new Vector3(-mapSize.x / 2f, -mapSize.y / 2f, 0);
			bulletWorldOrigin = new Vector3(mapWorldOrigin.x - 0.5f, mapWorldOrigin.y - 0.5f);
			var tankSize = (x: 2 * mapSize.x - 1, y: 2 * mapSize.y - 1);
			Tank.array = new List<Tank>[tankSize.x][];
			for (int x = 0; x < tankSize.x; ++x)
			{
				Tank.array[x] = new List<Tank>[tankSize.y];
				for (int y = 0; y < tankSize.y; ++y) Tank.array[x][y] = new List<Tank>();
			}

			var bulletSize = (x: tankSize.x + 2, y: tankSize.y + 2);
			Bullet.array = new List<Bullet>[bulletSize.x][];
			for (int x = 0; x < bulletSize.x; ++x)
			{
				Bullet.array[x] = new List<Bullet>[bulletSize.y];
				for (int y = 0; y < bulletSize.y; ++y) Bullet.array[x][y] = new List<Bullet>();
			}
			MIN_BULLET_WORLD = Vector3Int.zero.BulletArray_To_BulletWorld();
			MAX_BULLET_WORLD = new Vector3Int(bulletSize.x - 1, bulletSize.y - 1, 0).BulletArray_To_BulletWorld();
		}


		public static Vector3 MIN_BULLET_WORLD { get; private set; }

		public static Vector3 MAX_BULLET_WORLD { get; private set; }



		public static Vector3 TerrainArray_To_TerrainWorld(this Vector3Int a) => a + mapWorldOrigin;


		public static Vector3Int TerrainWorld_To_TerrainArray(this Vector3 w) => Vector3Int.FloorToInt(w - mapWorldOrigin);


		public static Vector3 TankArray_To_TankWorld(this Vector3Int a) => (Vector3)a * 0.5f + mapWorldOrigin;


		public static Vector3Int TankWorld_To_TankArray(this Vector3 w) => Vector3Int.FloorToInt((w - mapWorldOrigin) * 2);


		public static Vector3 BulletArray_To_BulletWorld(this Vector3Int a) => (Vector3)a * 0.5f + bulletWorldOrigin;


		public static Vector3Int BulletWorld_To_BulletArray(this Vector3 w) => Vector3Int.FloorToInt((w - bulletWorldOrigin) * 2);


		public static Vector3Int TerrainArray_To_TankArray(this Vector3Int terrain) => terrain * 2;


		public static Vector3 TerrainArray_To_TankWorld(this Vector3Int a) => a.TerrainArray_To_TankArray().TankArray_To_TankWorld();


		public static Vector3Int TerrainWorld_To_TankArray(this Vector3 w) => w.TerrainWorld_To_TerrainArray().TerrainArray_To_TankArray();


		public static Vector3Int? TankArray_To_TerrainArray(this Vector3Int tank) => (tank.x % 2 == 0 && tank.y % 2 == 0) ? new Vector3Int(tank.x / 2, tank.y / 2, 0) : (Vector3Int?)null;


		public static Vector3? TankArray_To_TerrainWorld(this Vector3Int a) => a.TankArray_To_TerrainArray()?.TerrainArray_To_TerrainWorld();


		public static Vector3? TankWorld_To_TerrainWorld(this Vector3 tank) => (tank == Vector3Int.FloorToInt(tank)) ? tank : (Vector3?)null;


		public static Vector3Int? TankWorld_To_TerrainArray(this Vector3 w) => w.TankWorld_To_TerrainWorld()?.TerrainWorld_To_TerrainArray();


		public static Vector3Int TankArray_To_BulletArray(this Vector3Int tank) => tank + new Vector3Int(1, 1, 0);


		public static Vector3 TankArray_To_BulletWorld(this Vector3Int a) => a.TankArray_To_BulletArray().BulletArray_To_BulletWorld();


		public static Vector3Int TankWorld_To_BulletArray(this Vector3 w) => w.BulletWorld_To_BulletArray();


		public static Vector3Int? BulletArray_To_TankArray(this Vector3Int bullet) => (bullet.x == 0 || bullet.y == 0 || bullet.x == Bullet.array.Length - 1 || bullet.y == Bullet.array[0].Length - 1) ? (Vector3Int?)null : bullet - new Vector3Int(1, 1, 0);


		public static Vector3? BulletArray_To_TankWorld(this Vector3Int bullet) => bullet.BulletArray_To_TankArray()?.TankArray_To_TankWorld();


		public static Vector3? BulletWorld_To_TankWorld(this Vector3 bullet) => (Mathf.Approximately(bullet.x, bulletWorldOrigin.x) || Mathf.Approximately(bullet.y, bulletWorldOrigin.y) || Mathf.Approximately(bullet.x, MAX_BULLET_WORLD.x) || Mathf.Approximately(bullet.y, MAX_BULLET_WORLD.y)) ?
			(Vector3?)null : bullet;


		public static Vector3Int? BulletWorld_To_TankArray(this Vector3 bullet) => bullet.BulletWorld_To_TankWorld()?.TankWorld_To_TankArray();


		public static Vector3Int TerrainArray_To_BulletArray(this Vector3Int terrain) => terrain.TerrainArray_To_TankArray().TankArray_To_BulletArray();


		public static Vector3 TerrainArray_To_BulletWorld(this Vector3Int terrain) => terrain.TerrainArray_To_BulletArray().BulletArray_To_BulletWorld();


		public static Vector3Int TerrainWorld_To_BulletArray(this Vector3 w) => w.BulletWorld_To_BulletArray();


		public static Vector3Int? BulletArray_To_TerrainArray(this Vector3Int bullet) => bullet.BulletArray_To_TankArray()?.TankArray_To_TerrainArray();


		public static Vector3? BulletArray_To_TerrainWorld(this Vector3Int a) => a.BulletArray_To_TerrainArray()?.TerrainArray_To_TerrainWorld();


		public static Vector3? BulletWorld_To_TerrainWorld(this Vector3 bullet) => bullet.BulletWorld_To_TankWorld()?.TankWorld_To_TerrainWorld();


		public static Vector3Int? BulletWorld_To_TerrainArray(this Vector3 w) => w.BulletWorld_To_TerrainWorld()?.TerrainWorld_To_TerrainArray();


		public static bool IsValidTerrainArray(this Vector3Int a) => 0 <= a.x && a.x < BattleTerrain.array.Length && 0 <= a.y && a.y < BattleTerrain.array[0].Length;


		public static bool IsValidTankArray(this Vector3Int a) => 0 <= a.x && a.x < Tank.array.Length && 0 <= a.y && a.y < Tank.array[0].Length;


		public static bool IsValidBulletArray(this Vector3Int a) => 0 <= a.x && a.x < Bullet.array.Length && 0 <= a.y && a.y < Bullet.array[0].Length;
		#endregion


		#region Direction
		private static readonly IReadOnlyDictionary<Direction, Vector3Int> direction_vector_dict = new Dictionary<Direction, Vector3Int>
		{
			[Direction.Left] = Vector3Int.left,
			[Direction.Up] = Vector3Int.up,
			[Direction.Right] = Vector3Int.right,
			[Direction.Down] = Vector3Int.down
		};

		public static Vector3Int ToUnitInt(this Direction d) => direction_vector_dict[d];


		public static Vector3 ToUnit(this Direction d) => direction_vector_dict[d];


		private static readonly IReadOnlyDictionary<Vector3Int, Direction> vector_direction_dict = new Dictionary<Vector3Int, Direction>
		{
			[Vector3Int.left] = Direction.Left,
			[Vector3Int.up] = Direction.Up,
			[Vector3Int.right] = Direction.Right,
			[Vector3Int.down] = Direction.Down
		};


		public static Direction ToDirection(this Vector3Int v) => vector_direction_dict[v];


		/// <summary>
		/// <see cref="Direction"/>.Left, <see cref="Direction"/>.Right, <see cref="Direction"/>.Up, <see cref="Direction"/>.Down
		/// </summary>
		public static readonly ReadOnlyArray<Direction> DIAGONAL_DIRECTIONS = new ReadOnlyArray<Direction>(new Direction[]
		{
			Direction.Left, Direction.Right, Direction.Up, Direction.Down
		});


		/// <summary>
		/// <see cref="Vector3Int"/>.left, <see cref="Vector3Int"/>.right, <see cref="Vector3Int"/>.up, <see cref="Vector3Int"/>.down
		/// </summary>
		public static readonly ReadOnlyArray<Vector3Int> DIAGONAL_VECTORS = new ReadOnlyArray<Vector3Int>(new Vector3Int[]
		{
			Vector3Int.left, Vector3Int.right, Vector3Int.up, Vector3Int.down
		});


		/// <summary>
		/// <see cref="Vector3Int"/>.left, <see cref="Vector3Int"/>.left + <see cref="Vector3Int"/>.up, <see cref="Vector3Int"/>.up  ........ 
		/// </summary>
		public static readonly ReadOnlyArray<Vector3Int> EIGHT_VECTORS = new ReadOnlyArray<Vector3Int>(new Vector3Int[]
		{
			Vector3Int.left, Vector3Int.left+Vector3Int.up, Vector3Int.up, Vector3Int.right+Vector3Int.up,
			Vector3Int.right, Vector3Int.right+Vector3Int.down, Vector3Int.down, Vector3Int.left+Vector3Int.down
		});
		#endregion


		public static bool Contains<T>(this IReadOnlyCollection<T> collection, T item) => (collection as ICollection<T>).Contains(item);


		#region Async Util
		public static readonly CancellationTokenSource CANCELED_SOURCE = new CancellationTokenSource();

		static Extensions()
		{
			CANCELED_SOURCE.Cancel();
		}


		public static async void CheckException(this UniTask task) => await task;

		public static async void CheckException<T>(this UniTask<T> task) => await task;
		#endregion
	}



	public enum Direction
	{
		Left, Up, Right, Down
	}



	[Serializable]
	public struct ReadOnlyArray<T> : IEnumerable<T>
	{
		private readonly T[] array;

		public ReadOnlyArray(T[] array) => this.array = array;

		public T this[int index] => array[index];

		public IEnumerator<T> GetEnumerator() => (array as IEnumerable<T>).GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => array.GetEnumerator();
	}
}