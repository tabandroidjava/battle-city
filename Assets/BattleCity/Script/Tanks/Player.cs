﻿using UnityEngine;
using UniRx.Async;
using System;
using System.Collections.Generic;
using sd = RotaryHeart.Lib.SerializableDictionary;
using BattleCity.BattleTerrains;
using BattleCity.Items;
using BattleCity.AI;
using System.Threading;


namespace BattleCity.Tanks
{
	public sealed class Player : Tank, IGamepadListener
	{
		public static readonly IReadOnlyList<Color> PLAYER_COLORS = new List<Color>()
		{
			Color.Yellow, Color.Green
		};


		#region Khởi tạo
		public struct InitData
		{
			public byte? star;
		}


		private void Init(InitData data)
		{
			if (data.star != null)
			{
				star = data.star.Value;
				if (!canBurnForest) fieryStar = (byte)star;
			}
		}


		[SerializeField] private int SHOOTING_DELAY_MILISECONDS_CONST = 150;
		private int _star;

		/// <summary>
		/// <code>
		/// switch (<see cref="star"/>)<br/>
		/// {<br/>
		///		case 0: // Bắn đạn chậm, đạn yếu nhất. Nếu bị trúng trực tiếp 1 viên đạn <see cref="Enemy"/> thì nổ<br/>
		///		case 1: // Bắn đạn nhanh hơn. Nếu bị trúng trực tiếp 1 viên đạn <see cref="Enemy"/> thì nổ<br/>
		///		case 2: // Bắn đạn nhanh bằng đạn "case 1", bắn đồng thời tối đa 2 viên.  Nếu bị trúng trực tiếp 1 viên đạn <see cref="Enemy"/> thì nổ<br/><br/>
		///		case 3: // (Có súng đại bác) Bắn đạn nhanh và đồng thời tối đa 2 viên như "case 2". 1 viên có thể phá 2 lớp <see cref="Brick"/> hoặc 1 lớp <see cref="Steel"/><br/>
		///			    // &gt;&gt;&gt; Nếu bị trúng trực tiếp 1 viên đạn <see cref="Enemy"/> thì <see cref="star"/> = 2<br/>
		/// }
		/// </code>
		/// </summary>
		public int star
		{
			get => _star;
			set
			{
				maxActiveBulletCount = (byte)(value > 1 ? 2 : 1);
				switch (value)
				{
					case 0: shootingDelayMiliseconds = SHOOTING_DELAY_MILISECONDS_CONST; break;
					case 1: shootingDelayMiliseconds = SHOOTING_DELAY_MILISECONDS_CONST - 20; break;
					case 2: shootingDelayMiliseconds = SHOOTING_DELAY_MILISECONDS_CONST - 40; break;
					case 3: shootingDelayMiliseconds = SHOOTING_DELAY_MILISECONDS_CONST - 70; break;
					default: if (value < 0) goto case 0; else goto case 3;
				}
				if (shootingDelayMiliseconds < 0) shootingDelayMiliseconds = 0;


				#region Cập nhật _star, cập nhật sprite và kiểm tra Explode
				if (value < 0)
				{
					_star = Setting.current.defaultPlayerStar;
					if (isActiveAndEnabled) Explode();
					return;
				}

				if (value > 3) value = 3;
				int old = _star;
				_star = value;
				switch (value)
				{
					case 0:
						if (old == 0) break;
						if (isActiveAndEnabled) Explode();
						break;

					case 1:
						switch (old)
						{
							case 0: spriteRenderer.sprite = sprites[value][color][direction]; break;
							case 1: break;
							case 2:
							case 3:
								if (isActiveAndEnabled) Explode();
								break;
						}
						break;

					case 2:
						switch (old)
						{
							case 0:
							case 1:
							case 3:
								spriteRenderer.sprite = sprites[value][color][direction];
								break;

							case 2: break;
						}
						break;

					case 3:
						if (old == 3) break;
						spriteRenderer.sprite = gunSprites[color][direction];
						break;
				}
				#endregion
			}
		}


		#region Xử lý canBurnForest : có thể bắn viên đạn đốt cháy cỏ (Forest) ?
		private bool canBurnForest;
		private byte _fireStar;

		/// <summary>
		/// <code>
		/// if (<see cref="fieryStar"/> == 0) <see cref="canBurnForest"/> = <see langword="false"/> <br/>
		/// else <see cref="canBurnForest"/>= ??? (phụ thuộc value hiện tại của <see cref="star"/> và <see cref="canBurnForest"/>)
		/// </code>
		/// </summary>
		public byte fieryStar
		{
			get => _fireStar;

			set
			{
				if (canBurnForest)
				{
					if (value >= 2)
					{
						canBurnForest = false;
						_fireStar = 0;
						return;
					}
				}
				else if (star == 3 && value >= 5)
				{
					canBurnForest = true;
					_fireStar = 0;
					return;
				}

				if ((_fireStar = value) == 0) canBurnForest = false;
			}
		}
		#endregion


		[NonSerialized] public Helmet helmet;

		private Direction _direction = Direction.Up;
		public override Direction direction
		{
			get => _direction;
			set
			{
				spriteRenderer.sprite = star == 3 ? gunSprites[color][value] : sprites[star][color][value];
				_direction = value;
			}
		}

		private Color _color;
		public override Color color
		{
			get => _color;
			set => throw new Exception("Cannot set Player.color !");
		}

		[SerializeField] private float MOVE_SPEED_CONST = 0.1f;


		private new void Awake()
		{
			base.Awake();
			moveSpeed = MOVE_SPEED_CONST;
		}
		#endregion


		private new void OnEnable()
		{
			Instantiate(Extensions.Load<Helmet>()).OnCollision(this);
			base.OnEnable();
			if (isActiveAndEnabled)
				if (BattleField.isGameOver) { if (PlayerAgent.agents[color]) PlayerAgent.agents[color].enabled = false; }
				else if (PlayerAgent.agents[color]) PlayerAgent.agents[color].enabled = true;
				else Gamepad.instances[(int)color].AddListener(this);
		}


		private new void OnDisable()
		{
			base.OnDisable();
			if (!PlayerAgent.agents[color]) Gamepad.instances[(int)color].RemoveListener(this);
		}


		public override async void Explode()
		{
			Effect.Play(Effect.Anim.BigExplosion, transform.position);
			Effect.Play(Effect.Sound.Player_Explosion);
			if (!PlayerAgent.agents[color]) Gamepad.instances[(int)color].Vibrate();
			Recycle();
			star = Setting.current.defaultPlayerStar;
			var ally = color == Color.Yellow ? Color.Green : Color.Yellow;
			if (lifes[color] == 0 && !players[ally] && lifes[ally] == 0 && !BattleField.isGameOver) await BattleField.Finish(isGameOver: true);
			else if (lifes[color] != 0) await Spawn(color, 1000);
			else if (PlayerAgent.agents[color] && lifes[ally] != 0) PlayerAgent.DecideBorrowingLife(color);
		}


		#region Object Pool
		[Serializable] private sealed class Int_Color_Direction_Sprite_Dict : sd.SerializableDictionaryBase<int, GlobalAsset.Color_Direction_Sprite_Dict> { }

		/// <summary>
		/// 0 &lt;= Int-key &lt;= 2
		/// </summary>
		[Tooltip("0 <= Int-key <= 2")]
		[SerializeField] private Int_Color_Direction_Sprite_Dict sprites;

		[Serializable] private sealed class Int_Color_Direction_Anim_Dict : sd.SerializableDictionaryBase<int, GlobalAsset.Color_Direction_Anim_Dict> { }

		/// <summary>
		/// 0 &lt;= Int-key &lt;= 2
		/// </summary>
		[Tooltip("0 <= Int-key <= 2")]
		[SerializeField] private Int_Color_Direction_Anim_Dict anims;

		protected override RuntimeAnimatorController GetMovingAnim => star == 3 ? gunAnims[color][direction] : anims[star][color][direction];

		public sealed class PlayerLife : Dictionary<Color, int>
		{
			public new int this[Color color]
			{
				get => base[color];
				set
				{
					base[color] = value;
					if (BattleField.instance) BattleField.instance.playerLifeTexts[color].text = value.ToString();
				}
			}
		}
		public static readonly PlayerLife lifes = new PlayerLife()
		{
			[Color.Yellow] = 0,
			[Color.Green] = 0
		};

		/// <summary>
		/// Các <see cref="Player"/> trong môi trường.<br/>
		/// <c>if <see cref="players"/>[i] != <see langword="null"/></c> : <see cref="players"/>[i] đang sống.
		/// </summary>
		public static readonly Dictionary<Color, Player> players = new Dictionary<Color, Player>()
		{
			[Color.Yellow] = null,
			[Color.Green] = null
		};
		private static readonly Dictionary<Color, Player> pool = new Dictionary<Color, Player>()
		{
			[Color.Yellow] = null,
			[Color.Green] = null
		};


		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void InitPool()
		{
			var player = pool[Color.Yellow] = Instantiate(Extensions.Load<Player>());
			player._color = Color.Yellow; player.name = "Yellow Player";
			DontDestroyOnLoad(player);

			player = pool[Color.Green] = Instantiate(Extensions.Load<Player>());
			player._color = Color.Green; player.name = "Green Player";
			DontDestroyOnLoad(player);
		}


		public static Player Get(Color color, InitData? data = null)
		{
			var player = players[color] = pool[color];
			pool[color] = null;
			if (data != null) player.Init(data.Value);
			return player;
		}


		public void Recycle()
		{
			gameObject.SetActive(false);
			players[color] = null;
			pool[color] = this;
		}


		/// <summary>
		/// <para>Chỉ kiểm tra và xử lý sau khi sync Physic.</para>
		/// Nếu tank đã tồn tại từ scene trước nhưng ẩn thì bật và cho vào game.
		/// <para>Nếu tank không tồn tại thì: mượn mạng tank đồng đội hoặc sinh với mạng của tank hiện tại.</para>
		/// <para>Return value có thể null !</para>
		/// </summary>
		public static async UniTask<Player> Spawn(Color color, int milisecondsDelay, InitData? data = null, bool borrowLife = false)
		{
			Effect.Play(Effect.Anim.Spawn, BattleField.battle.playerSpawnIndexes[color].TankArray_To_TankWorld());
			if (await UniTask.Delay(milisecondsDelay, delayTiming: PlayerLoopTiming.FixedUpdate, cancellationToken: BattleField.Token).SuppressCancellationThrow()
				|| (players[color] && players[color].isActiveAndEnabled)) return null;

			if (players[color])
				// Con tank đã tồn tại từ scene "BattleField" trước đó nhưng đang ẩn.
				// Chỉ bật vô môi trường game.
				return UpdateAndProcess();

			var ally = (color == Color.Yellow) ? Color.Green : Color.Yellow;
			if (borrowLife && !players[color] && lifes[color] == 0 && lifes[ally] != 0)
			{
				// Con tank không tồn tại (null) => Mượn mạng
				--lifes[ally];
				if (data?.star != null) Get(color, data);
				else Get(color, new InitData()
				{
					star = Setting.current.defaultPlayerStar
				});
				return UpdateAndProcess();
			}

			if (!borrowLife && !players[color] && lifes[color] != 0)
			{
				--lifes[color];
				Get(color);
				return UpdateAndProcess();
			}
			return null;


			Player UpdateAndProcess()
			{
				var player = players[color];
				player.index = BattleField.battle.playerSpawnIndexes[color];
				player.transform.position = player.index.TankArray_To_TankWorld();
				if (data != null) player.Init(data.Value);
				player.direction = Direction.Up;
				player.gameObject.SetActive(true);
				return player.isActiveAndEnabled ? player : null;
			}
		}
		#endregion


		#region OnCollision(Bullet)
		public override Bullet.CollisionResult OnCollision(Bullet bullet)
		{
			if (activeBullets.Contains(bullet)) return Bullet.CollisionResult.KeepBullet;
			if (helmet) return bullet.allyType == Bullet.AllyType.Enemy ? Bullet.CollisionResult.RemoveBullet
				 : Setting.current.playerBullet_Collise_player ? Bullet.CollisionResult.RemoveBullet : Bullet.CollisionResult.KeepBullet;

			if (bullet.allyType != Bullet.AllyType.Enemy)
			{
				if (Setting.current.playerBullet_Collise_player)
				{
					Effect.Play(Effect.Anim.SmallExplosion, bullet.transform.position);
					disableMoving_StopTime = Time.time + 5;
					if (disableMovingTask.IsCompleted) (disableMovingTask = DisableMoving()).CheckException();
					return Bullet.CollisionResult.RemoveBullet;
				}
				return Bullet.CollisionResult.KeepBullet;
			}

			if (ship)
			{
				Destroy(ship.gameObject);
				ship = null;
				return Bullet.CollisionResult.RemoveBullet;
			}

			fieryStar = 0;
			--star;
			if (isActiveAndEnabled)
			{
				Effect.Play(Effect.Anim.SmallExplosion, bullet.transform.position);
				Effect.Play(Effect.Sound.Bullet_Collise_Tank_But_Tank_Survive);
				if (!PlayerAgent.agents[color]) Gamepad.instances[(int)color].Vibrate(0.5f, 0.5f);
			}

			return Bullet.CollisionResult.RemoveBullet;
		}


		public UniTask disableMovingTask { get; private set; } = UniTask.CompletedTask;
		private float disableMoving_StopTime;

		private async UniTask DisableMoving()
		{
			var delay = UniTask.Delay(300, cancellationToken: Token).SuppressCancellationThrow();
			do
			{
				spriteRenderer.enabled = false;
				if (await delay) return;
				spriteRenderer.enabled = true;
				if (await delay) return;
			} while (Time.time < disableMoving_StopTime);
		}
		#endregion


		protected override void ExportBulletSpecificData(ref Bullet.InitData bullet)
		{
			bullet.moveSpeed = star == 0 ? 3 * moveSpeed : 6 * moveSpeed;
			bullet.canDestroySteel = star == 3;
			bullet.canBurnForest = canBurnForest;
		}


		/// <summary>
		/// Đóng băng Player ? (Không thể Move và Shoot ?)
		/// <para>Nếu <see cref="BattleField.isGameOver"/> == <see langword="true"/> : Bỏ qua</para>
		/// </summary>
		/// <param name="freeze"><see langword="true"/> : Đóng băng
		/// <para><see langword="false"/>: Hủy đóng băng</para>
		/// </param>
		public void Freeze(bool freeze)
		{
			if (BattleField.isGameOver) return;
			if (PlayerAgent.agents[color]) PlayerAgent.agents[color].enabled = !freeze;
			else if (freeze) Gamepad.instances[(int)color].RemoveListener(this);
			else Gamepad.instances[(int)color].AddListener(this);
		}


		public override async UniTask<(bool isSuccess, Bullet bullet)> Shoot()
		{
			var result = await base.Shoot();
			if (result.isSuccess) Effect.Play(Effect.Sound.Player_Shoot);
			return result;
		}


		public override async UniTask Move()
		{
			using (var cts = CancellationTokenSource.CreateLinkedTokenSource(Token, BattleField.Token))
			{
				var token = cts.Token;
				var yield = UniTask.Yield(PlayerLoopTiming.FixedUpdate, token).SuppressCancellationThrow();
				if ((await yield)) return;

				if (!canMove) return;
				var audioSource = Effect.Play(Effect.Sound.Player_Moving, loop: true);
				await (realMoveTask = RealMove(yield));
				Effect.Recycle(Effect.Sound.Player_Moving, audioSource);
				return;
			}
		}


		#region Gamepad
		public async void OnDpad(Direction direction, Gamepad.ButtonState state)
		{
			if (state == Gamepad.ButtonState.RELEASE || !moveTask.IsCompleted || !disableMovingTask.IsCompleted) return;

			var oldD = this.direction;
			this.direction = direction;
			if (state == Gamepad.ButtonState.PRESS && oldD != direction) return;
			await (moveTask = Move());
		}


		public Gamepad.LocalState state { get; } = new Gamepad.LocalState();

		public async void OnButtonA(Gamepad.ButtonState state)
		{
			if (state == Gamepad.ButtonState.PRESS && canShootBullets > 0)
			{
				var bullet = await Shoot();
			}
		}


		public void OnButtonB(Gamepad.ButtonState state) => OnButtonA(state);


		public async void OnButtonX(Gamepad.ButtonState state)
		{
			if (state != Gamepad.ButtonState.RELEASE && canShootBullets > 0)
				if (canShootBullets > 1) await UniTask.WhenAll(Shoot(), Shoot());
				else await Shoot();
		}


		public void OnButtonY(Gamepad.ButtonState state) => OnButtonX(state);

		public void OnButtonSelect() { }

		public void OnButtonStart() { }
		#endregion
	}
}