﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections.Generic;
using sd = RotaryHeart.Lib.SerializableDictionary;
using UniRx.Async;
using BattleCity.BattleTerrains;


namespace BattleCity.Tanks
{
	[RequireComponent(typeof(SpriteRenderer))]
	[DisallowMultipleComponent]
	public sealed class Bullet : MonoBehaviour, IBulletCollision
	{
		public const float COLLISION_RADIUS = 0.125F + 0.001F, COLLISION_DIAMETER_SQR = (0.25F + 0.002F) * (0.25F + 0.002F);


		#region Khởi tạo
		public static List<Bullet>[][] array;

		[Serializable] private sealed class Direction_Sprite_Dict : sd.SerializableDictionaryBase<Direction, Sprite> { }
		[SerializeField] private SpriteRenderer spriteRenderer;
		[SerializeField] private Direction_Sprite_Dict sprites;


		public struct InitData
		{
			#region Dữ liệu được xuất từ Tank tổng quát
			public Vector3 position;
			public Direction direction;
			public Vector3Int index;
			public AllyType allyType;
			public List<Bullet> activeBullets;
			#endregion

			#region Dữ liệu đặc biệt được xuất từ con Tank cụ thể
			/// <summary>
			/// 0 &lt; value &lt;= 0.125f
			/// </summary>
			public float moveSpeed;

			/// <summary>
			/// Có thể phá 1 lớp <see cref="Steel"/> hoặc 2 lớp <see cref="Brick"/>
			/// </summary>
			public bool canDestroySteel;
			public bool canBurnForest;
			#endregion
		}


		#region Dữ liệu được xuất từ Tank tổng quát
		public Vector3Int index { get; private set; }
		private Direction _direction;
		public Direction direction
		{
			get => _direction;
			private set => spriteRenderer.sprite = sprites[_direction = value];
		}

		public enum AllyType
		{
			YellowPlayer, GreenPlayer, Enemy
		}
		public AllyType allyType { get; private set; }
		private List<Bullet> activeBullets;
		#endregion


		#region Dữ liệu đặc biệt được xuất từ con Tank cụ thể
		/// <summary>
		/// 0 &lt; moveStep &lt;= 0.125f
		/// </summary>
		private float moveSpeed = 0.25f;

		/// <summary>
		/// Có thể phá 1 lớp <see cref="Steel"/> hoặc tối đa 2 lớp <see cref="Brick"/>
		/// </summary>
		public bool canDestroySteel { get; private set; }
		public bool canBurnForest { get; private set; }
		#endregion
		#endregion


		private async void OnEnable()
		{
			activeBullets.Add(this);
			array[index.x][index.y].Add(this);
			cancelMoving = new CancellationTokenSource();
			await Move();
		}


		private void OnDisable()
		{
			cancelMoving.Cancel();
			cancelMoving.Dispose();
			array[index.x][index.y].Remove(this);
			activeBullets.Remove(this);
			used.Remove(this);
			free.Add(this);
		}


		private void OnDestroy()
		{
			if (free.Count != 0) free.Clear();
		}


		private static readonly List<Bullet> used = new List<Bullet>(), free = new List<Bullet>();

		/// <summary>
		/// Return value có thể <see langword="null"/>
		/// </summary>
		internal static Bullet Spawn(InitData data)
		{
			Bullet bullet;
			if (free.Count != 0)
			{
				bullet = free[0]; free.RemoveAt(0);
			}
			else bullet = Instantiate(Extensions.Load<Bullet>(), BattleField.instance.bulletAnchor);
			used.Add(bullet);
			bullet.transform.position = data.position;
			bullet.index = data.index;
			bullet.direction = data.direction;
			bullet.moveSpeed = data.moveSpeed;
			bullet.allyType = data.allyType;
			bullet.canDestroySteel = data.canDestroySteel;
			bullet.canBurnForest = data.canBurnForest;
			bullet.activeBullets = data.activeBullets;

			bullet.gameObject.SetActive(true);
			return bullet.isActiveAndEnabled ? bullet : null;
		}


		#region Move
		private CancellationTokenSource cancelMoving;
		private static readonly Vector3Int[] LURD_vectors = new Vector3Int[]
		{
			Vector3Int.left,
			Vector3Int.up,
			Vector3Int.right,
			Vector3Int.down
		},
		cross_vectors = new Vector3Int[]
		{
			Vector3Int.left+Vector3Int.up,
			Vector3Int.right+Vector3Int.up,
			Vector3Int.right+Vector3Int.down,
			Vector3Int.left+Vector3Int.down
		};
		private static readonly List<IBulletCollision> checkingTerrains = new List<IBulletCollision>();
		private static readonly List<Bullet> checkingBullets = new List<Bullet>();
		private static readonly List<Tank> checkingTanks = new List<Tank>();


		private async UniTask Move()
		{
			var unitDir05 = direction.ToUnit() * 0.5f;
			var unitDirInt = direction.ToUnitInt();
			var stop = index.BulletArray_To_BulletWorld();
			if (stop != transform.position) stop += unitDir05;

			using (var cts = CancellationTokenSource.CreateLinkedTokenSource(cancelMoving.Token, BattleField.Token))
			{
				var yield = UniTask.Yield(PlayerLoopTiming.FixedUpdate, cts.Token).SuppressCancellationThrow();
				do
				{
					var p = transform.position;
					if (p == stop)
					{
						transform.position = p = stop;
						array[index.x][index.y].Remove(this);
						index = stop.BulletWorld_To_BulletArray();
						array[index.x][index.y].Add(this);
					}
					var result = CollisionResult.KeepBullet;

					#region Kiểm tra va chạm
					#region Kiểm tra va chạm biên
					if (p.x == Extensions.MIN_BULLET_WORLD.x || p.x == Extensions.MAX_BULLET_WORLD.x || p.y == Extensions.MIN_BULLET_WORLD.y || p.y == Extensions.MAX_BULLET_WORLD.y)
					{
						Effect.Play(Effect.Anim.SmallExplosion, p);
						if (allyType != AllyType.Enemy) Effect.Play(Effect.Sound.Bullet_Collise_Wall);
						break;
					}
					#endregion


					#region Kiểm tra va chạm BattleTerrain
					if (p == stop)
					{
						checkingTerrains.Clear();
						bool evenX = index.x % 2 == 0, evenY = index.y % 2 == 0;
						if (!evenX && !evenY)
							// Vị trí tương đối Bullet - Terrain: GIỮA TERRAIN
							TryAddTerrainFromBulletIndex(index);
						else if (evenX && evenY)
							// Vị trí tương đối Bullet - Terrain: GÓC TERRAIN
							foreach (var dir in cross_vectors)
							{
								var i = index + dir;
								if (i.IsValidBulletArray()) TryAddTerrainFromBulletIndex(i);
							}
						else
							// Vị trí tương đối Bullet - Terrain: CẠNH TERRAIN
							foreach (var dir in LURD_vectors)
							{
								var i = index + dir;
								if (i.IsValidBulletArray()) TryAddTerrainFromBulletIndex(i);
							}

						foreach (var terrain in checkingTerrains)
							if (terrain.OnCollision(this) == CollisionResult.RemoveBullet) result = CollisionResult.RemoveBullet;


						void TryAddTerrainFromBulletIndex(Vector3Int bulletIndex)
						{
							var index = bulletIndex.BulletArray_To_TerrainArray();
							if (index != null)
							{
								var t = BattleTerrain.array[index.Value.x][index.Value.y];
								if (t) checkingTerrains.Add(t);
							}
						}
					}
					#endregion


					#region Kiểm tra va chạm Bullet
					void TryAddSurroundBulletList(Vector3Int bulletIndex, Vector3Int ignored)
					{
						foreach (var dir in LURD_vectors)
						{
							var index = bulletIndex + dir;
							if (index != ignored && index.IsValidBulletArray())
								checkingBullets.AddRange(array[index.x][index.y]);
						}
					}


					checkingBullets.Clear();
					checkingBullets.AddRange(array[index.x][index.y]);
					var s = index + direction.ToUnitInt();
					checkingBullets.AddRange(array[s.x][s.y]);

					if ((index.BulletArray_To_BulletWorld() - p).sqrMagnitude <= COLLISION_DIAMETER_SQR)
						// Thêm 3 điểm array bên index (nếu tồn tại)
						TryAddSurroundBulletList(index, s);
					else if ((p - s.BulletArray_To_BulletWorld()).sqrMagnitude <= COLLISION_DIAMETER_SQR)
						// Thêm 3 điểm array bên s (nếu tồn tại)
						TryAddSurroundBulletList(s, index);

					foreach (var bullet in checkingBullets)
						if (!activeBullets.Contains(bullet)
							&& ((bullet.transform.position - p).sqrMagnitude <= COLLISION_DIAMETER_SQR)
							&& (bullet.OnCollision(this) == CollisionResult.RemoveBullet))
							result = CollisionResult.RemoveBullet;
					#endregion


					#region Kiểm tra va chạm Tank
					void TryAddTank(int minX, int maxX, int minY, int maxY, Vector3Int origin)
					{
						var dir = new Vector3Int();
						for (dir.x = minX; dir.x <= maxX; ++dir.x)
							for (dir.y = minY; dir.y <= maxY; ++dir.y)
							{
								var i = origin + dir;
								if (i.IsValidBulletArray())
								{
									var t = i.BulletArray_To_TankArray();
									if (t != null) checkingTanks.AddRange(Tank.array[t.Value.x][t.Value.y]);
								}
							}
					}


					checkingTanks.Clear();
					Vector3Int a, b;
					switch (direction)
					{
						case Direction.Left:
						case Direction.Right:
						{
							(a, b) = direction == Direction.Right ? (index, index + unitDirInt) : (index + unitDirInt, index);

							// Thêm tank ở khu vực giữa a và b
							TryAddTank(0, 1, -2, 2, a);

							if ((p - a.BulletArray_To_BulletWorld()).sqrMagnitude <= Tank.BULLET_TANK_MAX_COLLISION_DISTANCE_SQR)
								// p gần a có thể chạm tank ở khu vực bên a
								TryAddTank(-2, -1, -2, 2, a);
							else if ((p - b.BulletArray_To_BulletWorld()).sqrMagnitude <= Tank.BULLET_TANK_MAX_COLLISION_DISTANCE_SQR)
								// p gần b có thể chạm tank ở khu vực bên b
								TryAddTank(2, 3, -2, 2, a);
						}
						break;

						case Direction.Up:
						case Direction.Down:
						{
							(a, b) = direction == Direction.Up ? (index, index + unitDirInt) : (index + unitDirInt, index);

							// Thêm tank ở khu vực giữa a và b
							TryAddTank(-2, 2, 0, 1, a);

							if ((p - a.BulletArray_To_BulletWorld()).sqrMagnitude <= Tank.BULLET_TANK_MAX_COLLISION_DISTANCE_SQR)
								// p gần a có thể chạm tank ở khu vực bên a
								TryAddTank(-2, 2, -2, -1, a);
							else if ((p - b.BulletArray_To_BulletWorld()).sqrMagnitude <= Tank.BULLET_TANK_MAX_COLLISION_DISTANCE_SQR)
								// p gần b có thể chạm tank ở khu vực bên b
								TryAddTank(-2, 2, 2, 3, a);
						}
						break;
					}
					foreach (var tank in checkingTanks)
						if (((p - tank.transform.position).sqrMagnitude <= Tank.BULLET_TANK_MAX_COLLISION_DISTANCE_SQR)
							&& (tank.OnCollision(this) == CollisionResult.RemoveBullet))
							result = CollisionResult.RemoveBullet;
					#endregion
					#endregion

					if (result == CollisionResult.RemoveBullet) break;
					transform.position = Vector3.MoveTowards(p, p == stop ? stop += unitDir05 : stop, moveSpeed);
				} while (!await yield);
				if (!yield.IsCompleted || !yield.Result) gameObject.SetActive(false);
			}
		}
		#endregion


		public enum CollisionResult
		{
			KeepBullet, RemoveBullet
		}


		public CollisionResult OnCollision(Bullet bullet)
		{
			if (allyType == AllyType.Enemy && bullet.allyType == AllyType.Enemy) return CollisionResult.KeepBullet;
			gameObject.SetActive(false);
			return CollisionResult.RemoveBullet;
		}
	}



	public interface IBulletCollision
	{
		Bullet.CollisionResult OnCollision(Bullet bullet);
	}
}