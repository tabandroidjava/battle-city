﻿using UnityEngine;
using UniRx.Async;
using System.Collections.Generic;
using sd = RotaryHeart.Lib.SerializableDictionary;
using System;
using UnityEngine.SceneManagement;
using BattleCity.Items;
using BattleCity.AI;


namespace BattleCity.Tanks
{
	[RequireComponent(typeof(EnemyAgent))]
	public sealed class Enemy : Tank
	{
		#region Khởi tạo
		public struct InitData
		{
			public Vector3Int index;
			public Color color;
			public Type type;
			public Dictionary<Color, int> colorHP;
		}


		private static readonly Color[] ALL_COLORS = (Color[])Enum.GetValues(typeof(Color));

		private void Init(InitData data)
		{
			index = data.index;
			transform.position = index.TankArray_To_TankWorld();
			color = data.color;
			type = data.type;
			direction = Direction.Down;
			foreach (var color in ALL_COLORS) colorHP[color] = data.colorHP.ContainsKey(color) ? data.colorHP[color] : 0;
		}

		[Serializable] private sealed class Type_Color_Direction_Sprite_Dict : sd.SerializableDictionaryBase<Type, GlobalAsset.Color_Direction_Sprite_Dict> { }
		[SerializeField] private Type_Color_Direction_Sprite_Dict sprites;
		[Serializable] private sealed class Type_Color_Direction_Anim_Dict : sd.SerializableDictionaryBase<Type, GlobalAsset.Color_Direction_Anim_Dict> { }
		[SerializeField] private Type_Color_Direction_Anim_Dict anims;

		protected override RuntimeAnimatorController GetMovingAnim => weapon == Weapon.Gun ? gunAnims[color][direction] : anims[type][color][direction];

		public enum Type
		{
			Small, Fast, Big, Armored
		}
		private Type _type;
		public Type type
		{
			get => _type;
			private set
			{
				if (isActiveAndEnabled) throw new Exception("Khi Enemy đang sống (enabled) thì không thể set type !");
				_type = value;
				spriteRenderer.sprite = sprites[value][color][direction];
				var stat = Extensions.Load<GlobalAsset>().enemyStat;
				moveSpeed = stat.moveSpeed[value];
				shootingDelayMiliseconds = Mathf.RoundToInt(stat.delayShootSeconds[value] * 1000);
			}
		}

		private Direction _direction = Direction.Down;
		public override Direction direction
		{
			get => _direction;
			set
			{
				_direction = value;
				spriteRenderer.sprite = weapon == Weapon.Gun ? gunSprites[color][_direction]
					: sprites[type][color][_direction];
			}
		}

		/// <summary>
		/// Các <see cref="Enemy"/> đang sống trong môi trường.<br/>
		/// </summary>
		public static readonly List<Enemy> enemies = new List<Enemy>();

		private Color _color;
		public override Color color
		{
			get => _color;
			set
			{
				_color = value;
				spriteRenderer.sprite = weapon == Weapon.Gun ? gunSprites[value][direction] : sprites[type][value][direction];
				if (ship) ship.SetActiveShip(value);
			}
		}

		public readonly Dictionary<Color, int> colorHP = new Dictionary<Color, int>()
		{
			[Color.White] = 0,
			[Color.Yellow] = 0,
			[Color.Green] = 0,
			[Color.Red] = 0
		};

		public enum Weapon
		{
			Normal, Star, Gun
		}
		private Weapon _weapon;
		public Weapon weapon
		{
			get => _weapon;
			set
			{
				switch (_weapon = value)
				{
					case Weapon.Star:
						if (isActiveAndEnabled && value == Weapon.Normal) throw new Exception("Enemy đang có Weapon.Star không thể về Weapon.Normal khi đang sống !");
						goto case Weapon.Normal;

					case Weapon.Gun:
						if (isActiveAndEnabled && value != Weapon.Gun) throw new Exception("Enemy đang có Weapon.Gun không thể thay đổi Weapon khác khi đang sống !");
						goto case Weapon.Normal;

					case Weapon.Normal:
						spriteRenderer.sprite = value == Weapon.Gun ? gunSprites[color][direction] : sprites[type][color][direction];
						break;
				}
				moveSpeed = value == Weapon.Gun ? Extensions.Load<GlobalAsset>().enemyStat.moveSpeed[Type.Armored] : Extensions.Load<GlobalAsset>().enemyStat.moveSpeed[type];
			}
		}

		public EnemyAgent agent { get; private set; }


		private new void Awake()
		{
			base.Awake();
			transform.parent = BattleField.instance.noShipEnemyAnchor;
			agent = GetComponent<EnemyAgent>();
		}
		#endregion


		#region Object Pool
		public sealed class EnemyLife : Dictionary<Type, int>
		{
			/// <summary>
			/// Tổng số lượng <see cref="Enemy"/> dự trữ (chưa spawn).
			/// <para>Value thay đổi khi set <see cref="lifes"/>[i] = x</para>
			/// </summary>
			public int totalLife { get; private set; }


			public new int this[Type type]
			{
				get => base[type];
				set
				{
					TryGetValue(type, out int old);
					totalLife += value - old;
					base[type] = value;
					if (BattleField.instance) BattleField.instance.enemyLifeUI.Update(totalLife);
				}
			}
		}
		public static readonly EnemyLife lifes = new EnemyLife()
		{
			[Type.Small] = 0,
			[Type.Big] = 0,
			[Type.Fast] = 0,
			[Type.Armored] = 0
		};
		private static readonly List<Enemy> used = new List<Enemy>(), free = new List<Enemy>();


		private new void OnEnable()
		{
			enemies.Add(this);
			base.OnEnable();
		}


		private new void OnDisable()
		{
			base.OnDisable();
			enemies.Remove(this);
			used.Remove(this);
			free.Add(this);
			weapon = Weapon.Normal;
		}


		private void OnDestroy()
		{
			if (used.Contains(this)) used.Remove(this);
			else free.Remove(this);
		}


		public static async UniTask<Enemy> Spawn(InitData data, int milisecondsDelay)
		{
			Effect.Play(Effect.Anim.Spawn, data.index.TankArray_To_TankWorld());
			if ((await UniTask.Delay(milisecondsDelay, delayTiming: PlayerLoopTiming.FixedUpdate, cancellationToken: BattleField.Token).SuppressCancellationThrow())
				|| lifes[data.type] == 0) return null;

			--lifes[data.type];
			Enemy enemy;
			if (free.Count != 0)
			{
				enemy = free[0]; free.RemoveAt(0);
			}
			else
			{
				enemy = Instantiate(Extensions.Load<Enemy>());
				enemy.transform.parent = BattleField.instance.noShipEnemyAnchor;
			}
			used.Add(enemy);
			enemy.Init(data);
			enemy.gameObject.SetActive(true);
			return enemy.isActiveAndEnabled ? enemy : null;
		}
		#endregion


		public override async void Explode()
		{
			Effect.Play(Effect.Anim.BigExplosion, transform.position);
			Effect.Play(Effect.Sound.Enemy_Explosion);
			gameObject.SetActive(false);
			bool hasLife = false;
			foreach (var kvp in lifes) if (kvp.Value != 0) { hasLife = true; break; }
			if (!hasLife && enemies.Count == 0)
			{
				Effect.Recycle(Effect.Sound.Enemy_Exist);
				if (!BattleField.isGameOver) await BattleField.Finish(isGameOver: false);
				return;
			}

			if (hasLife) await EnemyAgent.SpawnEnemy();
		}


		public override Bullet.CollisionResult OnCollision(Bullet bullet)
		{
			if (bullet.allyType == Bullet.AllyType.Enemy || activeBullets.Contains(bullet)) return Bullet.CollisionResult.KeepBullet;
			var originalColor = color;

			#region Cập nhật colorHP và color
			if (--colorHP[color] <= 0)
				switch (color)
				{
					case Color.Red:
						color = colorHP[Color.Green] > 0 ? Color.Green : colorHP[Color.Yellow] > 0 ? Color.Yellow : Color.White;
						break;

					case Color.Green:
						color = colorHP[Color.Yellow] > 0 ? Color.Yellow : Color.White;
						break;

					case Color.Yellow:
						color = Color.White;
						break;
				}
			#endregion

			if (originalColor == Color.Red) Item.Generate();
			if (colorHP[color] <= 0 && weapon != Weapon.Gun) ++ScoreBoard.killedEnemies[bullet.allyType == Bullet.AllyType.YellowPlayer ? Color.Yellow : Color.Green][type];
			if (!isActiveAndEnabled) return Bullet.CollisionResult.RemoveBullet;

			#region Kiểm tra và xử lý nếu bị nổ hoặc vẫn còn sống
			if (colorHP[color] <= 0)
			{
				if (ship)
				{
					Destroy(ship.gameObject);
					ship = null;
					SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
					transform.parent = BattleField.instance.noShipEnemyAnchor;
				}
				Explode();
			}
			else
			{
				Effect.Play(Effect.Anim.SmallExplosion, bullet.transform.position);
				Effect.Play(Effect.Sound.Bullet_Collise_Tank_But_Tank_Survive);
			}
			#endregion

			return Bullet.CollisionResult.RemoveBullet;
		}


		protected override void ExportBulletSpecificData(ref Bullet.InitData bullet)
		{
			bullet.moveSpeed = Extensions.Load<GlobalAsset>().enemyStat.bulletSpeed[type];
			bullet.canDestroySteel = weapon != Weapon.Normal;
			bullet.canBurnForest = false;
		}
	}
}