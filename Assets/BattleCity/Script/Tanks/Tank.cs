﻿using UnityEngine;
using System.Collections.Generic;
using UniRx.Async;
using System.Threading;
using System;
using BattleCity.Items;
using BattleCity.BattleTerrains;


namespace BattleCity.Tanks
{
	[RequireComponent(typeof(SpriteRenderer), typeof(Animator))]
	[DisallowMultipleComponent]
	public abstract class Tank : MonoBehaviour, IBulletCollision
	{
		protected const float COLLISION_RADIUS = 0.6035533905932738F + 0.001F;
		public const float BULLET_TANK_MAX_COLLISION_DISTANCE_SQR = Bullet.COLLISION_RADIUS * Bullet.COLLISION_RADIUS + 2 * Bullet.COLLISION_RADIUS * COLLISION_RADIUS + COLLISION_RADIUS * COLLISION_RADIUS;


		#region Khởi tạo
		public static List<Tank>[][] array;

		public enum Color : byte
		{
			Yellow, Green, White, Red
		}
		public Vector3Int index { get; protected set; }
		public abstract Color color { get; set; }
		public abstract Direction direction { get; set; }

		public float moveSpeed { get; protected set; }
		[NonSerialized] public Ship ship;
		public SpriteRenderer spriteRenderer;
		[SerializeField] private float DEFAULT_ANIMATOR_SPEED = 1;
		[SerializeField] protected Animator animator;

		protected static GlobalAsset.Color_Direction_Sprite_Dict gunSprites { get; private set; }
		protected static GlobalAsset.Color_Direction_Anim_Dict gunAnims { get; private set; }


		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void InitAsset()
		{
			var asset = Extensions.Load<GlobalAsset>();
			gunSprites = asset.tankGunSprites;
			gunAnims = asset.tankGunAnims;
		}


		protected void Awake()
		{
			animator.speed = DEFAULT_ANIMATOR_SPEED;
		}
		#endregion


		private CancellationTokenSource cancelAll;

		/// <summary>
		/// Khi Tank bị disable thì <code><see cref="Token"/>.IsCancellationRequested == <see langword="true"/></code>
		/// </summary>
		/// <exception cref="ObjectDisposedException">Tank đã disable</exception>
		public CancellationToken Token => cancelAll.Token;

		protected void OnEnable()
		{
			var t = index.TankArray_To_TerrainArray().Value;
			var terrain = BattleTerrain.array[t.x][t.y];
			if (terrain) Destroy(terrain.gameObject);
			array[index.x][index.y].Add(this);
			cancelAll = new CancellationTokenSource();
			spriteRenderer.enabled = true;
			if ((Setting.current.enemy_Collise_item || !(this is Enemy))
				&& (Item.current && Item.current.tankCollisionIndexes.Contains(index)))
			{
				Item.current.OnCollision(this);
				if (this is Player) ScoreBoard.IncreaseScore(color, Item.BONUS_SCORE);
				Effect.Play(Effect.Sound.Tank_Collise_Item);
			}
		}


		protected void OnDisable()
		{
			array[index.x][index.y].Remove(this);
			cancelAll.Cancel();
			cancelAll.Dispose();
		}


		#region Shoot
		/// <summary>
		/// value &gt;= 0
		/// </summary>
		protected int shootingDelayMiliseconds;
		protected readonly List<Bullet> activeBullets = new List<Bullet>();
		protected byte maxActiveBulletCount = 1;
		private byte shootTaskCount;

		/// <summary>
		/// Return: số viên đạn lý thuyết (theo trạng thái hiện tại) mà Tank có thể bắn.
		/// <para>0 &lt;= value &lt;= <see cref="maxActiveBulletCount"/></para>
		/// </summary>
		public int canShootBullets => maxActiveBulletCount - activeBullets.Count - shootTaskCount;


		public virtual async UniTask<(bool isSuccess, Bullet bullet)> Shoot()
		{
			++shootTaskCount;
			using (var cts = CancellationTokenSource.CreateLinkedTokenSource(cancelAll.Token, BattleField.Token))
			{
				var token = cts.Token;
				if (token.IsCancellationRequested) throw new Exception();
				if (await UniTask.Delay(shootingDelayMiliseconds, delayTiming: PlayerLoopTiming.FixedUpdate, cancellationToken: token).SuppressCancellationThrow()
					|| activeBullets.Count == maxActiveBulletCount)
				{
					--shootTaskCount;
					return (false, null);
				}

				var data = new Bullet.InitData()
				{
					position = transform.position + direction.ToUnit() * 0.5f,
					index = index.TankArray_To_BulletArray() + direction.ToUnitInt(),
					direction = direction,
					allyType = this is Player ? (color == Color.Yellow ? Bullet.AllyType.YellowPlayer : Bullet.AllyType.GreenPlayer) : Bullet.AllyType.Enemy,
					activeBullets = activeBullets
				};
				ExportBulletSpecificData(ref data);
				var bullet = Bullet.Spawn(data);
				--shootTaskCount;
				return (true, bullet);
			}
		}


		protected abstract void ExportBulletSpecificData(ref Bullet.InitData bullet);
		#endregion


		#region Move
		public UniTask moveTask = UniTask.CompletedTask;

		/// <summary>
		/// Nhớ gán: <see cref="moveTask"/> = <see cref="Move"/>
		/// </summary>
		/// <returns><see langword="true"/> : có thể move được và còn sống.</returns>
		public virtual async UniTask Move()
		{
			using (var cts = CancellationTokenSource.CreateLinkedTokenSource(cancelAll.Token, BattleField.Token))
			{
				var token = cts.Token;
				var yield = UniTask.Yield(PlayerLoopTiming.FixedUpdate, token).SuppressCancellationThrow();
				if ((await yield)) return;

				if (!canMove) return;
				await (realMoveTask = RealMove(yield));
				return;
			}
		}


		public UniTask realMoveTask { get; protected set; } = UniTask.CompletedTask;
		private static readonly List<BattleTerrain> checkingTerrains = new List<BattleTerrain>();

		/// <summary>
		/// Nhớ gán <see cref="realMoveTask"/> = <see cref="RealMove"/>
		/// </summary>
		protected async UniTask RealMove(UniTask<bool> yield)
		{
			var dir = direction.ToUnit() * 0.5f;
			var stop = transform.position + dir;
			var p = transform.position;

			animator.runtimeAnimatorController = GetMovingAnim;
			do
			{
				transform.position = p = Vector3.MoveTowards(p, stop, moveSpeed);
			} while (p != stop && !await yield);
			if (!yield.Result || this) animator.runtimeAnimatorController = null;

			if (!yield.Result)
			{
				transform.position = stop;
				array[index.x][index.y].Remove(this);
				index = stop.TankWorld_To_TankArray();
				array[index.x][index.y].Add(this);
				if ((Setting.current.enemy_Collise_item || !(this is Enemy))
					&& (Item.current && Item.current.tankCollisionIndexes.Contains(index)))
				{
					Item.current.OnCollision(this);
					if (this is Player) ScoreBoard.IncreaseScore(color, Item.BONUS_SCORE);
					Effect.Play(Effect.Sound.Tank_Collise_Item);
				}


				#region Xử lý va chạm BattleTerrain
				checkingTerrains.Clear();
				switch (direction)
				{
					case Direction.Left: TryAddTerrain(checkingTerrains, index, -1, 0, -1, 1); break;
					case Direction.Up: TryAddTerrain(checkingTerrains, index, -1, 1, 0, 1); break;
					case Direction.Right: TryAddTerrain(checkingTerrains, index, 0, 1, -1, 1); break;
					case Direction.Down: TryAddTerrain(checkingTerrains, index, -1, 1, -1, 0); break;
				}

				foreach (var terrain in checkingTerrains) (terrain as ITankCollision)?.OnCollision(this);
				#endregion
			}
		}

		protected abstract RuntimeAnimatorController GetMovingAnim { get; }

		#region canMove
		/// <summary>
		/// <see cref="Tank"/> tại trị trí <paramref name="index"/> có thể di chuyển 1 bước theo <paramref name="direction"/> ?
		/// <para><see cref="Tank"/> có thể bị cản bởi biên, <see cref="BattleTerrain"/> hoặc <see cref="Tank"/></para>
		/// <para>Chỉ kiểm tra khi <see cref="Tank"/> đang ở vị trí chuẩn: <see cref="Transform.position"/> == <see cref="index"/>.TankArray_To_TankWorld()</para>
		/// </summary>
		/// <param name="hasShip">Có thuyền ? (<see cref="ship"/> != <see langword="null"/>)</param>
		public static bool CanMove(Vector3Int index, Direction direction, bool hasShip) => (index + direction.ToUnitInt()).IsValidTankArray() && !IsBlockingMoveByBattleTerrain(index, direction, hasShip) && !IsBlockingMoveByTank(index, direction);


		#region IsBlockingMoveByBattleTerrain()
		/// <summary>
		/// Tìm các <see cref="BattleTerrain"/> có thể cản hành động <see cref="Tank"/> di chuyển 1 bước theo <paramref name="direction"/>
		/// </summary>
		public static void FindBlockable_BattleTerrain(Vector3Int index, Direction direction, List<BattleTerrain> result)
		{
			switch (direction)
			{
				case Direction.Left: TryAddTerrain(result, index, -2, -1, -1, 1); break;
				case Direction.Up: TryAddTerrain(result, index, -1, 1, 1, 2); break;
				case Direction.Right: TryAddTerrain(result, index, 1, 2, -1, 1); break;
				case Direction.Down: TryAddTerrain(result, index, -1, 1, -2, -1); break;
			}
		}


		/// <summary>
		/// <see cref="Tank"/> bị chặn không thể di chuyển 1 bước bởi <see cref="BattleTerrain"/> ?
		/// </summary>
		/// <param name="hasShip">Có thuyền ? (<see cref="ship"/> != <see langword="null"/>)</param>
		public static bool IsBlockingMoveByBattleTerrain(Vector3Int index, Direction direction, bool hasShip)
		{
			checkingTerrains.Clear();
			FindBlockable_BattleTerrain(index, direction, checkingTerrains);
			foreach (var terrain in checkingTerrains) if (terrain.IsBlockingTankMove(index, direction, hasShip)) return true;
			return false;
		}
		#endregion


		private static readonly List<Rect> checkingRects = new List<Rect>();

		/// <summary>
		/// <see cref="Tank"/> bị chặn không thể di chuyển 1 bước bởi <see cref="Tank"/> ?
		/// </summary>
		public static bool IsBlockingMoveByTank(Vector3Int index, Direction direction)
		{
			void TryAddIndexList(int minX, int maxX, int minY, int maxY)
			{
				var dir = new Vector3Int();
				for (dir.x = minX; dir.x <= maxX; ++dir.x)
					for (dir.y = minY; dir.y <= maxY; ++dir.y)
					{
						var t = index + dir;
						if (t.IsValidTankArray())
							foreach (var tank in array[t.x][t.y])
								checkingRects.Add(new Rect()
								{
									size = tank.realMoveTask.IsCompleted ? new Vector2(2, 2) : tank.direction == Direction.Left || tank.direction == Direction.Right ? new Vector2(3, 2) : new Vector2(2, 3),
									center = tank.realMoveTask.IsCompleted ? t : t + tank.direction.ToUnit() * 0.5f,
								});
					}
			}


			checkingRects.Clear();
			switch (direction)
			{
				case Direction.Left: TryAddIndexList(-3, -2, -2, 2); break;
				case Direction.Up: TryAddIndexList(-2, 2, 2, 3); break;
				case Direction.Right: TryAddIndexList(2, 3, -2, 2); break;
				case Direction.Down: TryAddIndexList(-2, 2, -3, -2); break;
			}

			var dest = index + direction.ToUnitInt();
			var destRect = new Rect()
			{
				size = new Vector2(2, 2),
				center = new Vector2(dest.x, dest.y)
			};
			foreach (var rect in checkingRects) if (destRect.Overlaps(rect)) return true;
			return false;
		}


		public bool canMove => CanMove(index, direction, ship);
		#endregion


		private static void TryAddTerrain(List<BattleTerrain> list, Vector3Int index, int minX, int maxX, int minY, int maxY)
		{
			var dir = new Vector3Int();
			for (dir.x = minX; dir.x <= maxX; ++dir.x)
				for (dir.y = minY; dir.y <= maxY; ++dir.y)
				{
					var i = index + dir;
					if (i.IsValidTankArray())
					{
						var t = i.TankArray_To_TerrainArray();
						if (t != null)
						{
							var terrain = BattleTerrain.array[t.Value.x][t.Value.y];
							if (terrain) list.Add(terrain);
						}
					}
				}
		}
		#endregion


		public abstract void Explode();

		public abstract Bullet.CollisionResult OnCollision(Bullet bullet);
	}



	public interface ITankCollision
	{
		void OnCollision(Tank tank);
	}
}