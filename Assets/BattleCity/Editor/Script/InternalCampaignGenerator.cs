﻿using UnityEngine;
using UnityEditor;
using BattleCity.Util;


namespace BattleCity.Editors
{
	internal sealed class InternalCampaignGenerator : Editor
	{
		[MenuItem("Tools/Generate InternalCampaign asset")]
		private static void Generate()
		{
			AssetDatabase.DeleteAsset("Assets/BattleCity/Internal Campaign.asset");
			var ic = AssetDatabase.LoadAssetAtPath<InternalCampaign>("Assets/BattleCity/Editor/Internal Campaign.prefab");
			AssetDatabase.CreateAsset(new TextAsset(ic.ToCampaign().ToString()), "Assets/BattleCity/Internal Campaign.asset");
			AssetDatabase.SaveAssets();
			Debug.Log("Saved Asset !");
		}
	}
}